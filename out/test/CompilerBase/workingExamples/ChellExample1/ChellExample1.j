.bytecode 52.0
.source ChellExample1.java
.class public ChellExample1
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

ldc "string to be printed"
astore 1
ldc 49
istore 2
ldc 1
istore 3
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 2
invokevirtual java/io/PrintStream/println(I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 3
invokevirtual java/io/PrintStream/println(Z)V
ldc "this is another line"
astore 4
ldc 7
istore 5
ldc 1
istore 6
ldc 31
ldc 20
if_icmpne L2
ldc 1
goto L3
L2:
ldc 0
L3:
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L1
L0:
iload 5
iload 5
imul
iload 2
if_icmpeq L5
ldc 1
goto L6
L5:
ldc 0
L6:
ifeq L4I0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 4
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L1
L4I0:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "YOU ARE THE WINNER!!!!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
iload 6
iload 3
iand
ldc 1
goto L10
L9:
ldc 0
L10:
ifeq L7
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "YOU ARE THE WINNER!!!!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L8
L7:
L8:
iload 6
iload 3
ior
ldc 1
goto L14
L13:
ldc 0
L14:
ifeq L11
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "YOU ARE THE WINNER!!!!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L12
L11:
L12:
return
.end method

