.bytecode 52.0
.source ChellExample6.java
.class public ChellExample6
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

invokestatic ChellExample6/space()Z
ifeq L0
ldc "smurf"
ldc 0
invokestatic ChellExample6/icecream(Ljava/lang/String;I)V
goto L1
L0:
L1:

return
.end method


.method public static space()Z
.limit stack 99
.limit locals 99

ldc 1
istore 1
iload 1

ireturn
.end method


.method public static icecream(Ljava/lang/String;I)V
.limit stack 99
.limit locals 99

getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "icecream: "
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 3
invokevirtual java/io/PrintStream/println(I)V

return
.end method


