package nl.saxion.cos;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CompilerTest {

	private String pathNameGenerator(String fileName) {
		return "testFiles/workingExamples/" + fileName + "/" + fileName + ".chell";
	}

	@Test
	void PrintSomething() throws Exception {
		String filename = "ChellExample0";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);

		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"Test 1337"
		}, output.toArray());
	}

	@Test
	void PrintSomethingMoreAndUseIfStatments() throws Exception {
		String filename = "ChellExample1";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"string to be printed",
				"49",
				"true",
				"YOU ARE THE WINNER!!!!",
				"YOU ARE THE WINNER!!!!",
				"YOU ARE THE WINNER!!!!"
		}, output.toArray());
	}

	@Test
	void SimpelIfStatement() throws Exception {
		String filename = "ChellExample4";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"Text123"
		}, output.toArray());
	}

	@Test
	void SimpleDeclarationAndPrinting() throws Exception {
		String filename = "ChellExample9";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"Hello World!",
				"10",
				"true"
		}, output.toArray());
	}

	@Test
	void MediumMethodTest() throws Exception {
		String filename = "ChellExample3";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"icecream: ",
				"Smurf",
				"icecream: ",
				"Smurf",
				"icecream: ",
				"Smurf",
				"icecream: ",
				"Smurf",
				"icecream: ",
				"Smurf"
		}, output.toArray());
	}

	@Test
	void SimpleMethodTest() throws Exception {
		String filename = "ChellExample5";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"This should be printed!",
				"This should also be printed!"
		}, output.toArray());
	}

	@Test
	void SimpleForLoopTest() throws Exception {
		String filename = "ChellExample7";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"Banaan",
				"Banaan",
				"Banaan",
				"Banaan",
				"Banaan",
				"Banaan",
				"Banaan",
		}, output.toArray());
	}

	@Test
	void AdvancedMethodTest() throws Exception {
		String filename = "ChellExample8";
		Compiler c = new Compiler();
		JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
		AssembledClass aClass = AssembledClass.assemble(code);

		SandBox s = new SandBox();
		s.runClass(aClass);
		List<String> output = s.getOutput();

		assertArrayEquals(new String[] {
				"GOTTA GOT TO SPACE!!!",
				"GOTTA GOT TO SPACE!!!",
				"GOTTA GOT TO SPACE!!!",
				"GOTTA GOT TO SPACE!!!",
				"GOTTA GOT TO SPACE!!!",
				"GOTTA GOT TO SPACE!!!"
		}, output.toArray());
	}

//	@Test
//	void syntaxErrorsAreFound() throws Exception {
//		Compiler c = new Compiler();
//		JasminBytecode code = c.compileString("dfsgkjs;", "HelloWorld");
//		assertNull(code);
//	}
}
