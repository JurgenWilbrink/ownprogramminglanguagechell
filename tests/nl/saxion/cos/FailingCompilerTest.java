package nl.saxion.cos;

import nl.saxion.cos.exceptions.CompilerException;
import nl.saxion.cos.exceptions.ReturnTypesDontMatchException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class FailingCompilerTest {

    private String pathNameGenerator(String fileName) {
        return "testFiles/failingExamples/" + fileName + "/" + fileName + ".chell";
    }

    @Test
    void returnTypesDontMatchException() throws Exception {
        String filename = "ChellFailingExample0";
        Compiler c = new Compiler();
        assertThrows(ReturnTypesDontMatchException.class, () -> {
            JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
        });
    }

    @Test
    void variableDoesntExist() throws Exception {
        String filename = "ChellFailingExample1";
        Compiler c = new Compiler();
        assertThrows(CompilerException.class, () -> {
            JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
        });
    }

    @Test
    void whileStatementNeedsABooleanInsteadOfInt() throws Exception {
        String filename = "ChellFailingExample2";
        Compiler c = new Compiler();
        assertThrows(CompilerException.class, () -> {
            JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
        });
    }

    @Test
    void methodDoesntExist() throws Exception {
        String filename = "ChellFailingExample3";
        Compiler c = new Compiler();
        assertThrows(CompilerException.class, () -> {
            JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
        });
    }

    @Test
    void stringsAndIntsCantBeCountedTogether() throws Exception {
        String filename = "ChellFailingExample4";
        Compiler c = new Compiler();
        assertThrows(CompilerException.class, () -> {
            JasminBytecode code = c.compileFile(pathNameGenerator(filename), filename);
        });
    }

}