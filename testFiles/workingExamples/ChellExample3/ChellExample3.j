.bytecode 52.0
.source ChellExample3.java
.class public ChellExample3
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

invokestatic ChellExample3/space()Z
ifeq L0
ldc "smurf"
ldc 0
invokestatic ChellExample3/icecream(Ljava/lang/String;I)V
goto L1
L0:
L1:

return
.end method


.method public static space()Z
.limit stack 99
.limit locals 99

ldc 1
istore 0
iload 0

ireturn
.end method


.method public static icecream(Ljava/lang/String;I)V
.limit stack 99
.limit locals 99

L2:
iload 1
ldc 5
if_icmpeq L4
ldc 1
goto L5
L4:
ldc 0
L5:
ifeq L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "icecream: "
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 1
ldc 1
iadd
istore 1
goto L2
L3:

return
.end method


