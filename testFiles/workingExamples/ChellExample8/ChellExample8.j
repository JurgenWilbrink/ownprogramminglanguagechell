.bytecode 52.0
.source ChellExample8.java
.class public ChellExample8
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

invokestatic ChellExample8/yesyesyes()Z
ifeq L0
invokestatic ChellExample8/number()I
invokestatic ChellExample8/iwantThree()I
invokestatic ChellExample8/calculate(II)I
invokestatic ChellExample8/screamSpace(I)V
goto L1
L0:
L1:

return
.end method


.method public static yesyesyes()Z
.limit stack 99
.limit locals 99

ldc 1
istore 0
iload 0

ireturn
.end method


.method public static iwantThree()I
.limit stack 99
.limit locals 99

ldc 3

ireturn
.end method


.method public static number()I
.limit stack 99
.limit locals 99

ldc 1
istore 0
ldc 2
istore 1
iload 0
iload 1
imul

ireturn
.end method


.method public static calculate(II)I
.limit stack 99
.limit locals 99

iload 0
iload 1
imul
istore 2
iload 2

ireturn
.end method


.method public static screamSpace(I)V
.limit stack 99
.limit locals 99

ldc 0
istore 1
L2:
iload 1
iload 0
if_icmpeq L4
ldc 1
goto L5
L4:
ldc 0
L5:
ifeq L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "GOTTA GOT TO SPACE!!!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 1
ldc 1
iadd
istore 1
goto L2
L3:

return
.end method


