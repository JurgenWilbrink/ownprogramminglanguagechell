.bytecode 52.0
.source ChellExample7.java
.class public ChellExample7
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

ldc "Banaan"
astore 0
ldc 0
istore 1
ldc 7
istore 2
L0:
iload 1
iload 2
if_icmpeq L2
ldc 1
goto L3
L2:
ldc 0
L3:
ifeq L1
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 1
ldc 1
iadd
istore 1
goto L0
L1:

return
.end method


