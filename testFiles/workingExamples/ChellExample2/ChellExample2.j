.bytecode 52.0
.source ChellExample2.java
.class public ChellExample2
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "What should be printed?"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
invokestatic java/lang/System/console()Ljava/io/Console;
invokevirtual java/io/Console/readLine()Ljava/lang/String;
astore 0
ldc 5
istore 1
L0:
iload 1
ldc 0
if_icmpeq L2
ldc 1
goto L3
L2:
ldc 0
L3:
ifeq L1
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 1
ldc 1
isub
istore 1
ldc 2
istore 2
goto L0
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 2
invokevirtual java/io/PrintStream/println(I)V

return
.end method


