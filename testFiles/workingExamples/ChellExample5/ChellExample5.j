.bytecode 52.0
.source ChellExample5.java
.class public ChellExample5
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack 99
.limit locals 99

invokestatic ChellExample5/space()V
invokestatic ChellExample5/icecream()V

return
.end method


.method public static space()V
.limit stack 99
.limit locals 99

getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "This should be printed!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

return
.end method


.method public static icecream()V
.limit stack 99
.limit locals 99

getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "This should also be printed!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

return
.end method


