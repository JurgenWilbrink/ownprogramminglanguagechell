// Generated from C:/Users/jurge/Desktop/Java-programs/MyOwnLanguageChellMap/GitifiedChell/src\Chell.g4 by ANTLR 4.8
package nl.saxion.cos;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ChellParser}.
 */
public interface ChellListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ChellParser#project}.
	 * @param ctx the parse tree
	 */
	void enterProject(ChellParser.ProjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChellParser#project}.
	 * @param ctx the parse tree
	 */
	void exitProject(ChellParser.ProjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChellParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(ChellParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChellParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(ChellParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChellParser#method}.
	 * @param ctx the parse tree
	 */
	void enterMethod(ChellParser.MethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChellParser#method}.
	 * @param ctx the parse tree
	 */
	void exitMethod(ChellParser.MethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChellParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ChellParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChellParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ChellParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatDeclaration}
	 * labeled alternative in {@link ChellParser#setValueStatement}.
	 * @param ctx the parse tree
	 */
	void enterStatDeclaration(ChellParser.StatDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatDeclaration}
	 * labeled alternative in {@link ChellParser#setValueStatement}.
	 * @param ctx the parse tree
	 */
	void exitStatDeclaration(ChellParser.StatDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatReDeclaration}
	 * labeled alternative in {@link ChellParser#assignmentStatement}.
	 * @param ctx the parse tree
	 */
	void enterStatReDeclaration(ChellParser.StatReDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatReDeclaration}
	 * labeled alternative in {@link ChellParser#assignmentStatement}.
	 * @param ctx the parse tree
	 */
	void exitStatReDeclaration(ChellParser.StatReDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatDeclareVarName}
	 * labeled alternative in {@link ChellParser#declareVariableNameStatement}.
	 * @param ctx the parse tree
	 */
	void enterStatDeclareVarName(ChellParser.StatDeclareVarNameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatDeclareVarName}
	 * labeled alternative in {@link ChellParser#declareVariableNameStatement}.
	 * @param ctx the parse tree
	 */
	void exitStatDeclareVarName(ChellParser.StatDeclareVarNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatIf}
	 * labeled alternative in {@link ChellParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterStatIf(ChellParser.StatIfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatIf}
	 * labeled alternative in {@link ChellParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitStatIf(ChellParser.StatIfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatWhile}
	 * labeled alternative in {@link ChellParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterStatWhile(ChellParser.StatWhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatWhile}
	 * labeled alternative in {@link ChellParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitStatWhile(ChellParser.StatWhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatOut}
	 * labeled alternative in {@link ChellParser#outputStatement}.
	 * @param ctx the parse tree
	 */
	void enterStatOut(ChellParser.StatOutContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatOut}
	 * labeled alternative in {@link ChellParser#outputStatement}.
	 * @param ctx the parse tree
	 */
	void exitStatOut(ChellParser.StatOutContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExIdentifier}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExIdentifier(ChellParser.ExIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExIdentifier}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExIdentifier(ChellParser.ExIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExNegate}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExNegate(ChellParser.ExNegateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExNegate}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExNegate(ChellParser.ExNegateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExParentHaakjes}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExParentHaakjes(ChellParser.ExParentHaakjesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExParentHaakjes}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExParentHaakjes(ChellParser.ExParentHaakjesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExMethodCall}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExMethodCall(ChellParser.ExMethodCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExMethodCall}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExMethodCall(ChellParser.ExMethodCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExMulOp}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExMulOp(ChellParser.ExMulOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExMulOp}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExMulOp(ChellParser.ExMulOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExStrLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExStrLiteral(ChellParser.ExStrLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExStrLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExStrLiteral(ChellParser.ExStrLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExAddOp}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExAddOp(ChellParser.ExAddOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExAddOp}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExAddOp(ChellParser.ExAddOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExBoolLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExBoolLiteral(ChellParser.ExBoolLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExBoolLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExBoolLiteral(ChellParser.ExBoolLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExEqualsE}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExEqualsE(ChellParser.ExEqualsEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExEqualsE}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExEqualsE(ChellParser.ExEqualsEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExIntLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExIntLiteral(ChellParser.ExIntLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExIntLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExIntLiteral(ChellParser.ExIntLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExInput}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExInput(ChellParser.ExInputContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExInput}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExInput(ChellParser.ExInputContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChellParser#equalsSigning}.
	 * @param ctx the parse tree
	 */
	void enterEqualsSigning(ChellParser.EqualsSigningContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChellParser#equalsSigning}.
	 * @param ctx the parse tree
	 */
	void exitEqualsSigning(ChellParser.EqualsSigningContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChellParser#askForInputExpression}.
	 * @param ctx the parse tree
	 */
	void enterAskForInputExpression(ChellParser.AskForInputExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChellParser#askForInputExpression}.
	 * @param ctx the parse tree
	 */
	void exitAskForInputExpression(ChellParser.AskForInputExpressionContext ctx);
}