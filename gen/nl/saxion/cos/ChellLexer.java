// Generated from C:/Users/jurge/Desktop/Java-programs/MyOwnLanguageChellMap/GitifiedChell/src\Chell.g4 by ANTLR 4.8
package nl.saxion.cos;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ChellLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BOOLEAN=1, INT=2, STRING=3, TYPENAME=4, BOOLEANNAME=5, INTNAME=6, STRINGNAME=7, 
		RETURN=8, EQUALS=9, NOTEQ=10, AND=11, OR=12, BT=13, LT=14, BTEQ=15, LTEQ=16, 
		SS=17, ME=18, PM=19, ML=20, SPACE=21, EQSIGN=22, PLUS=23, MINUS=24, MINUSWITHOUTSPACE=25, 
		COLON=26, PARENTBRACKET=27, ROUNDBRACKETOPEN=28, ROUNDBRACKETCLOSE=29, 
		COMMA=30, CREATEMETHOD=31, SHOULDREPLYWITH=32, SETPERSONALITYWITHSETTINGS=33, 
		PORTAL=34, NEXTPORTAL=35, NOWIWANTYOUGONE=36, TURRETKEEPSONSHOOTINGUNTIL=37, 
		IVEGOTANOTEHERE=38, PROGRAMSTART=39, PROGRAMEND=40, ASKFORSTRINGINPUT=41, 
		NAME=42, WS=43, COMMENT=44;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"BOOLEAN", "INT", "STRING", "TYPENAME", "BOOLEANNAME", "INTNAME", "STRINGNAME", 
			"RETURN", "EQUALS", "NOTEQ", "AND", "OR", "BT", "LT", "BTEQ", "LTEQ", 
			"SS", "ME", "PM", "ML", "SPACE", "EQSIGN", "PLUS", "MINUS", "MINUSWITHOUTSPACE", 
			"COLON", "PARENTBRACKET", "ROUNDBRACKETOPEN", "ROUNDBRACKETCLOSE", "COMMA", 
			"CREATEMETHOD", "SHOULDREPLYWITH", "SETPERSONALITYWITHSETTINGS", "PORTAL", 
			"NEXTPORTAL", "NOWIWANTYOUGONE", "TURRETKEEPSONSHOOTINGUNTIL", "IVEGOTANOTEHERE", 
			"PROGRAMSTART", "PROGRAMEND", "ASKFORSTRINGINPUT", "NAME", "WS", "COMMENT"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, "'cake'", "'potatos'", "'notes'", "'chapter3 '", 
			"'is not a lie'", "'is a lie'", "'end'", "'ore'", "'higher level of'", 
			"'lower level of'", "'higher then or on level'", "'lower then or on level'", 
			"'?'", "'##'", null, "'*'", "' '", null, null, null, "'-'", "':'", "'|'", 
			"'('", "')'", null, "'create core'", "'should reply with'", "'set personnality with settings'", 
			"'portal'", "'next portal'", "'now i want you gone'", "'turret keeps on shooting until'", 
			"'Ive got a note here'", "'IM MAKING A NOTE HERE'", "'This was a triumph'", 
			"'Give me some notes'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "BOOLEAN", "INT", "STRING", "TYPENAME", "BOOLEANNAME", "INTNAME", 
			"STRINGNAME", "RETURN", "EQUALS", "NOTEQ", "AND", "OR", "BT", "LT", "BTEQ", 
			"LTEQ", "SS", "ME", "PM", "ML", "SPACE", "EQSIGN", "PLUS", "MINUS", "MINUSWITHOUTSPACE", 
			"COLON", "PARENTBRACKET", "ROUNDBRACKETOPEN", "ROUNDBRACKETCLOSE", "COMMA", 
			"CREATEMETHOD", "SHOULDREPLYWITH", "SETPERSONALITYWITHSETTINGS", "PORTAL", 
			"NEXTPORTAL", "NOWIWANTYOUGONE", "TURRETKEEPSONSHOOTINGUNTIL", "IVEGOTANOTEHERE", 
			"PROGRAMSTART", "PROGRAMEND", "ASKFORSTRINGINPUT", "NAME", "WS", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ChellLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Chell.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2.\u0222\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2e\n\2\3\3\3\3\5\3i\n"+
		"\3\3\3\3\3\7\3m\n\3\f\3\16\3p\13\3\5\3r\n\3\3\4\3\4\7\4v\n\4\f\4\16\4"+
		"y\13\4\3\4\3\4\3\5\3\5\3\5\5\5\u0080\n\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3"+
		"\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\23\3\23\3\23"+
		"\3\24\3\24\5\24\u0112\n\24\3\25\3\25\3\26\3\26\3\27\3\27\3\27\3\27\3\30"+
		"\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35"+
		"\3\35\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3!"+
		"\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3\"\3\"\3\"\3\"\3"+
		"\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\""+
		"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$"+
		"\3$\3$\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%"+
		"\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&"+
		"\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3"+
		"\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3"+
		"(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3)\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3"+
		"*\3*\3*\3*\3*\3*\3*\3*\3+\6+\u0205\n+\r+\16+\u0206\3,\6,\u020a\n,\r,\16"+
		",\u020b\3,\3,\3-\3-\3-\3-\3-\3-\3-\7-\u0217\n-\f-\16-\u021a\13-\3-\6-"+
		"\u021d\n-\r-\16-\u021e\3-\3-\3\u0218\2.\3\3\5\4\7\5\t\6\13\7\r\b\17\t"+
		"\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27"+
		"-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W"+
		"-Y.\3\2\b\3\2\63;\3\2\62;\t\2\"#..\62<AAC\\aac|\5\2C\\aac|\5\2\13\f\17"+
		"\17\"\"\4\2\f\f\17\17\2\u022d\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2"+
		"\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2"+
		"+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2"+
		"\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2"+
		"C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3"+
		"\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\3d\3\2\2"+
		"\2\5q\3\2\2\2\7s\3\2\2\2\t\177\3\2\2\2\13\u0081\3\2\2\2\r\u0086\3\2\2"+
		"\2\17\u008e\3\2\2\2\21\u0094\3\2\2\2\23\u009e\3\2\2\2\25\u00ab\3\2\2\2"+
		"\27\u00b4\3\2\2\2\31\u00b8\3\2\2\2\33\u00bc\3\2\2\2\35\u00cc\3\2\2\2\37"+
		"\u00db\3\2\2\2!\u00f3\3\2\2\2#\u010a\3\2\2\2%\u010c\3\2\2\2\'\u0111\3"+
		"\2\2\2)\u0113\3\2\2\2+\u0115\3\2\2\2-\u0117\3\2\2\2/\u011b\3\2\2\2\61"+
		"\u011f\3\2\2\2\63\u0123\3\2\2\2\65\u0125\3\2\2\2\67\u0127\3\2\2\29\u0129"+
		"\3\2\2\2;\u012b\3\2\2\2=\u012d\3\2\2\2?\u0130\3\2\2\2A\u013c\3\2\2\2C"+
		"\u014e\3\2\2\2E\u016d\3\2\2\2G\u0174\3\2\2\2I\u0180\3\2\2\2K\u0194\3\2"+
		"\2\2M\u01b3\3\2\2\2O\u01c7\3\2\2\2Q\u01dd\3\2\2\2S\u01f0\3\2\2\2U\u0204"+
		"\3\2\2\2W\u0209\3\2\2\2Y\u020f\3\2\2\2[\\\7h\2\2\\]\7c\2\2]^\7n\2\2^_"+
		"\7u\2\2_e\7g\2\2`a\7v\2\2ab\7t\2\2bc\7w\2\2ce\7g\2\2d[\3\2\2\2d`\3\2\2"+
		"\2e\4\3\2\2\2fr\7\62\2\2gi\7/\2\2hg\3\2\2\2hi\3\2\2\2ij\3\2\2\2jn\t\2"+
		"\2\2km\t\3\2\2lk\3\2\2\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2or\3\2\2\2pn\3\2"+
		"\2\2qf\3\2\2\2qh\3\2\2\2r\6\3\2\2\2sw\7$\2\2tv\t\4\2\2ut\3\2\2\2vy\3\2"+
		"\2\2wu\3\2\2\2wx\3\2\2\2xz\3\2\2\2yw\3\2\2\2z{\7$\2\2{\b\3\2\2\2|\u0080"+
		"\5\r\7\2}\u0080\5\17\b\2~\u0080\5\13\6\2\177|\3\2\2\2\177}\3\2\2\2\177"+
		"~\3\2\2\2\u0080\n\3\2\2\2\u0081\u0082\7e\2\2\u0082\u0083\7c\2\2\u0083"+
		"\u0084\7m\2\2\u0084\u0085\7g\2\2\u0085\f\3\2\2\2\u0086\u0087\7r\2\2\u0087"+
		"\u0088\7q\2\2\u0088\u0089\7v\2\2\u0089\u008a\7c\2\2\u008a\u008b\7v\2\2"+
		"\u008b\u008c\7q\2\2\u008c\u008d\7u\2\2\u008d\16\3\2\2\2\u008e\u008f\7"+
		"p\2\2\u008f\u0090\7q\2\2\u0090\u0091\7v\2\2\u0091\u0092\7g\2\2\u0092\u0093"+
		"\7u\2\2\u0093\20\3\2\2\2\u0094\u0095\7e\2\2\u0095\u0096\7j\2\2\u0096\u0097"+
		"\7c\2\2\u0097\u0098\7r\2\2\u0098\u0099\7v\2\2\u0099\u009a\7g\2\2\u009a"+
		"\u009b\7t\2\2\u009b\u009c\7\65\2\2\u009c\u009d\7\"\2\2\u009d\22\3\2\2"+
		"\2\u009e\u009f\7k\2\2\u009f\u00a0\7u\2\2\u00a0\u00a1\7\"\2\2\u00a1\u00a2"+
		"\7p\2\2\u00a2\u00a3\7q\2\2\u00a3\u00a4\7v\2\2\u00a4\u00a5\7\"\2\2\u00a5"+
		"\u00a6\7c\2\2\u00a6\u00a7\7\"\2\2\u00a7\u00a8\7n\2\2\u00a8\u00a9\7k\2"+
		"\2\u00a9\u00aa\7g\2\2\u00aa\24\3\2\2\2\u00ab\u00ac\7k\2\2\u00ac\u00ad"+
		"\7u\2\2\u00ad\u00ae\7\"\2\2\u00ae\u00af\7c\2\2\u00af\u00b0\7\"\2\2\u00b0"+
		"\u00b1\7n\2\2\u00b1\u00b2\7k\2\2\u00b2\u00b3\7g\2\2\u00b3\26\3\2\2\2\u00b4"+
		"\u00b5\7g\2\2\u00b5\u00b6\7p\2\2\u00b6\u00b7\7f\2\2\u00b7\30\3\2\2\2\u00b8"+
		"\u00b9\7q\2\2\u00b9\u00ba\7t\2\2\u00ba\u00bb\7g\2\2\u00bb\32\3\2\2\2\u00bc"+
		"\u00bd\7j\2\2\u00bd\u00be\7k\2\2\u00be\u00bf\7i\2\2\u00bf\u00c0\7j\2\2"+
		"\u00c0\u00c1\7g\2\2\u00c1\u00c2\7t\2\2\u00c2\u00c3\7\"\2\2\u00c3\u00c4"+
		"\7n\2\2\u00c4\u00c5\7g\2\2\u00c5\u00c6\7x\2\2\u00c6\u00c7\7g\2\2\u00c7"+
		"\u00c8\7n\2\2\u00c8\u00c9\7\"\2\2\u00c9\u00ca\7q\2\2\u00ca\u00cb\7h\2"+
		"\2\u00cb\34\3\2\2\2\u00cc\u00cd\7n\2\2\u00cd\u00ce\7q\2\2\u00ce\u00cf"+
		"\7y\2\2\u00cf\u00d0\7g\2\2\u00d0\u00d1\7t\2\2\u00d1\u00d2\7\"\2\2\u00d2"+
		"\u00d3\7n\2\2\u00d3\u00d4\7g\2\2\u00d4\u00d5\7x\2\2\u00d5\u00d6\7g\2\2"+
		"\u00d6\u00d7\7n\2\2\u00d7\u00d8\7\"\2\2\u00d8\u00d9\7q\2\2\u00d9\u00da"+
		"\7h\2\2\u00da\36\3\2\2\2\u00db\u00dc\7j\2\2\u00dc\u00dd\7k\2\2\u00dd\u00de"+
		"\7i\2\2\u00de\u00df\7j\2\2\u00df\u00e0\7g\2\2\u00e0\u00e1\7t\2\2\u00e1"+
		"\u00e2\7\"\2\2\u00e2\u00e3\7v\2\2\u00e3\u00e4\7j\2\2\u00e4\u00e5\7g\2"+
		"\2\u00e5\u00e6\7p\2\2\u00e6\u00e7\7\"\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9"+
		"\7t\2\2\u00e9\u00ea\7\"\2\2\u00ea\u00eb\7q\2\2\u00eb\u00ec\7p\2\2\u00ec"+
		"\u00ed\7\"\2\2\u00ed\u00ee\7n\2\2\u00ee\u00ef\7g\2\2\u00ef\u00f0\7x\2"+
		"\2\u00f0\u00f1\7g\2\2\u00f1\u00f2\7n\2\2\u00f2 \3\2\2\2\u00f3\u00f4\7"+
		"n\2\2\u00f4\u00f5\7q\2\2\u00f5\u00f6\7y\2\2\u00f6\u00f7\7g\2\2\u00f7\u00f8"+
		"\7t\2\2\u00f8\u00f9\7\"\2\2\u00f9\u00fa\7v\2\2\u00fa\u00fb\7j\2\2\u00fb"+
		"\u00fc\7g\2\2\u00fc\u00fd\7p\2\2\u00fd\u00fe\7\"\2\2\u00fe\u00ff\7q\2"+
		"\2\u00ff\u0100\7t\2\2\u0100\u0101\7\"\2\2\u0101\u0102\7q\2\2\u0102\u0103"+
		"\7p\2\2\u0103\u0104\7\"\2\2\u0104\u0105\7n\2\2\u0105\u0106\7g\2\2\u0106"+
		"\u0107\7x\2\2\u0107\u0108\7g\2\2\u0108\u0109\7n\2\2\u0109\"\3\2\2\2\u010a"+
		"\u010b\7A\2\2\u010b$\3\2\2\2\u010c\u010d\7%\2\2\u010d\u010e\7%\2\2\u010e"+
		"&\3\2\2\2\u010f\u0112\5/\30\2\u0110\u0112\5\61\31\2\u0111\u010f\3\2\2"+
		"\2\u0111\u0110\3\2\2\2\u0112(\3\2\2\2\u0113\u0114\7,\2\2\u0114*\3\2\2"+
		"\2\u0115\u0116\7\"\2\2\u0116,\3\2\2\2\u0117\u0118\5+\26\2\u0118\u0119"+
		"\7?\2\2\u0119\u011a\5+\26\2\u011a.\3\2\2\2\u011b\u011c\5+\26\2\u011c\u011d"+
		"\7-\2\2\u011d\u011e\5+\26\2\u011e\60\3\2\2\2\u011f\u0120\5+\26\2\u0120"+
		"\u0121\7/\2\2\u0121\u0122\5+\26\2\u0122\62\3\2\2\2\u0123\u0124\7/\2\2"+
		"\u0124\64\3\2\2\2\u0125\u0126\7<\2\2\u0126\66\3\2\2\2\u0127\u0128\7~\2"+
		"\2\u01288\3\2\2\2\u0129\u012a\7*\2\2\u012a:\3\2\2\2\u012b\u012c\7+\2\2"+
		"\u012c<\3\2\2\2\u012d\u012e\7.\2\2\u012e\u012f\5+\26\2\u012f>\3\2\2\2"+
		"\u0130\u0131\7e\2\2\u0131\u0132\7t\2\2\u0132\u0133\7g\2\2\u0133\u0134"+
		"\7c\2\2\u0134\u0135\7v\2\2\u0135\u0136\7g\2\2\u0136\u0137\7\"\2\2\u0137"+
		"\u0138\7e\2\2\u0138\u0139\7q\2\2\u0139\u013a\7t\2\2\u013a\u013b\7g\2\2"+
		"\u013b@\3\2\2\2\u013c\u013d\7u\2\2\u013d\u013e\7j\2\2\u013e\u013f\7q\2"+
		"\2\u013f\u0140\7w\2\2\u0140\u0141\7n\2\2\u0141\u0142\7f\2\2\u0142\u0143"+
		"\7\"\2\2\u0143\u0144\7t\2\2\u0144\u0145\7g\2\2\u0145\u0146\7r\2\2\u0146"+
		"\u0147\7n\2\2\u0147\u0148\7{\2\2\u0148\u0149\7\"\2\2\u0149\u014a\7y\2"+
		"\2\u014a\u014b\7k\2\2\u014b\u014c\7v\2\2\u014c\u014d\7j\2\2\u014dB\3\2"+
		"\2\2\u014e\u014f\7u\2\2\u014f\u0150\7g\2\2\u0150\u0151\7v\2\2\u0151\u0152"+
		"\7\"\2\2\u0152\u0153\7r\2\2\u0153\u0154\7g\2\2\u0154\u0155\7t\2\2\u0155"+
		"\u0156\7u\2\2\u0156\u0157\7q\2\2\u0157\u0158\7p\2\2\u0158\u0159\7p\2\2"+
		"\u0159\u015a\7c\2\2\u015a\u015b\7n\2\2\u015b\u015c\7k\2\2\u015c\u015d"+
		"\7v\2\2\u015d\u015e\7{\2\2\u015e\u015f\7\"\2\2\u015f\u0160\7y\2\2\u0160"+
		"\u0161\7k\2\2\u0161\u0162\7v\2\2\u0162\u0163\7j\2\2\u0163\u0164\7\"\2"+
		"\2\u0164\u0165\7u\2\2\u0165\u0166\7g\2\2\u0166\u0167\7v\2\2\u0167\u0168"+
		"\7v\2\2\u0168\u0169\7k\2\2\u0169\u016a\7p\2\2\u016a\u016b\7i\2\2\u016b"+
		"\u016c\7u\2\2\u016cD\3\2\2\2\u016d\u016e\7r\2\2\u016e\u016f\7q\2\2\u016f"+
		"\u0170\7t\2\2\u0170\u0171\7v\2\2\u0171\u0172\7c\2\2\u0172\u0173\7n\2\2"+
		"\u0173F\3\2\2\2\u0174\u0175\7p\2\2\u0175\u0176\7g\2\2\u0176\u0177\7z\2"+
		"\2\u0177\u0178\7v\2\2\u0178\u0179\7\"\2\2\u0179\u017a\7r\2\2\u017a\u017b"+
		"\7q\2\2\u017b\u017c\7t\2\2\u017c\u017d\7v\2\2\u017d\u017e\7c\2\2\u017e"+
		"\u017f\7n\2\2\u017fH\3\2\2\2\u0180\u0181\7p\2\2\u0181\u0182\7q\2\2\u0182"+
		"\u0183\7y\2\2\u0183\u0184\7\"\2\2\u0184\u0185\7k\2\2\u0185\u0186\7\"\2"+
		"\2\u0186\u0187\7y\2\2\u0187\u0188\7c\2\2\u0188\u0189\7p\2\2\u0189\u018a"+
		"\7v\2\2\u018a\u018b\7\"\2\2\u018b\u018c\7{\2\2\u018c\u018d\7q\2\2\u018d"+
		"\u018e\7w\2\2\u018e\u018f\7\"\2\2\u018f\u0190\7i\2\2\u0190\u0191\7q\2"+
		"\2\u0191\u0192\7p\2\2\u0192\u0193\7g\2\2\u0193J\3\2\2\2\u0194\u0195\7"+
		"v\2\2\u0195\u0196\7w\2\2\u0196\u0197\7t\2\2\u0197\u0198\7t\2\2\u0198\u0199"+
		"\7g\2\2\u0199\u019a\7v\2\2\u019a\u019b\7\"\2\2\u019b\u019c\7m\2\2\u019c"+
		"\u019d\7g\2\2\u019d\u019e\7g\2\2\u019e\u019f\7r\2\2\u019f\u01a0\7u\2\2"+
		"\u01a0\u01a1\7\"\2\2\u01a1\u01a2\7q\2\2\u01a2\u01a3\7p\2\2\u01a3\u01a4"+
		"\7\"\2\2\u01a4\u01a5\7u\2\2\u01a5\u01a6\7j\2\2\u01a6\u01a7\7q\2\2\u01a7"+
		"\u01a8\7q\2\2\u01a8\u01a9\7v\2\2\u01a9\u01aa\7k\2\2\u01aa\u01ab\7p\2\2"+
		"\u01ab\u01ac\7i\2\2\u01ac\u01ad\7\"\2\2\u01ad\u01ae\7w\2\2\u01ae\u01af"+
		"\7p\2\2\u01af\u01b0\7v\2\2\u01b0\u01b1\7k\2\2\u01b1\u01b2\7n\2\2\u01b2"+
		"L\3\2\2\2\u01b3\u01b4\7K\2\2\u01b4\u01b5\7x\2\2\u01b5\u01b6\7g\2\2\u01b6"+
		"\u01b7\7\"\2\2\u01b7\u01b8\7i\2\2\u01b8\u01b9\7q\2\2\u01b9\u01ba\7v\2"+
		"\2\u01ba\u01bb\7\"\2\2\u01bb\u01bc\7c\2\2\u01bc\u01bd\7\"\2\2\u01bd\u01be"+
		"\7p\2\2\u01be\u01bf\7q\2\2\u01bf\u01c0\7v\2\2\u01c0\u01c1\7g\2\2\u01c1"+
		"\u01c2\7\"\2\2\u01c2\u01c3\7j\2\2\u01c3\u01c4\7g\2\2\u01c4\u01c5\7t\2"+
		"\2\u01c5\u01c6\7g\2\2\u01c6N\3\2\2\2\u01c7\u01c8\7K\2\2\u01c8\u01c9\7"+
		"O\2\2\u01c9\u01ca\7\"\2\2\u01ca\u01cb\7O\2\2\u01cb\u01cc\7C\2\2\u01cc"+
		"\u01cd\7M\2\2\u01cd\u01ce\7K\2\2\u01ce\u01cf\7P\2\2\u01cf\u01d0\7I\2\2"+
		"\u01d0\u01d1\7\"\2\2\u01d1\u01d2\7C\2\2\u01d2\u01d3\7\"\2\2\u01d3\u01d4"+
		"\7P\2\2\u01d4\u01d5\7Q\2\2\u01d5\u01d6\7V\2\2\u01d6\u01d7\7G\2\2\u01d7"+
		"\u01d8\7\"\2\2\u01d8\u01d9\7J\2\2\u01d9\u01da\7G\2\2\u01da\u01db\7T\2"+
		"\2\u01db\u01dc\7G\2\2\u01dcP\3\2\2\2\u01dd\u01de\7V\2\2\u01de\u01df\7"+
		"j\2\2\u01df\u01e0\7k\2\2\u01e0\u01e1\7u\2\2\u01e1\u01e2\7\"\2\2\u01e2"+
		"\u01e3\7y\2\2\u01e3\u01e4\7c\2\2\u01e4\u01e5\7u\2\2\u01e5\u01e6\7\"\2"+
		"\2\u01e6\u01e7\7c\2\2\u01e7\u01e8\7\"\2\2\u01e8\u01e9\7v\2\2\u01e9\u01ea"+
		"\7t\2\2\u01ea\u01eb\7k\2\2\u01eb\u01ec\7w\2\2\u01ec\u01ed\7o\2\2\u01ed"+
		"\u01ee\7r\2\2\u01ee\u01ef\7j\2\2\u01efR\3\2\2\2\u01f0\u01f1\7I\2\2\u01f1"+
		"\u01f2\7k\2\2\u01f2\u01f3\7x\2\2\u01f3\u01f4\7g\2\2\u01f4\u01f5\7\"\2"+
		"\2\u01f5\u01f6\7o\2\2\u01f6\u01f7\7g\2\2\u01f7\u01f8\7\"\2\2\u01f8\u01f9"+
		"\7u\2\2\u01f9\u01fa\7q\2\2\u01fa\u01fb\7o\2\2\u01fb\u01fc\7g\2\2\u01fc"+
		"\u01fd\7\"\2\2\u01fd\u01fe\7p\2\2\u01fe\u01ff\7q\2\2\u01ff\u0200\7v\2"+
		"\2\u0200\u0201\7g\2\2\u0201\u0202\7u\2\2\u0202T\3\2\2\2\u0203\u0205\t"+
		"\5\2\2\u0204\u0203\3\2\2\2\u0205\u0206\3\2\2\2\u0206\u0204\3\2\2\2\u0206"+
		"\u0207\3\2\2\2\u0207V\3\2\2\2\u0208\u020a\t\6\2\2\u0209\u0208\3\2\2\2"+
		"\u020a\u020b\3\2\2\2\u020b\u0209\3\2\2\2\u020b\u020c\3\2\2\2\u020c\u020d"+
		"\3\2\2\2\u020d\u020e\b,\2\2\u020eX\3\2\2\2\u020f\u0210\7d\2\2\u0210\u0211"+
		"\7w\2\2\u0211\u0212\7t\2\2\u0212\u0213\7p\2\2\u0213\u0214\7<\2\2\u0214"+
		"\u0218\3\2\2\2\u0215\u0217\13\2\2\2\u0216\u0215\3\2\2\2\u0217\u021a\3"+
		"\2\2\2\u0218\u0219\3\2\2\2\u0218\u0216\3\2\2\2\u0219\u021c\3\2\2\2\u021a"+
		"\u0218\3\2\2\2\u021b\u021d\t\7\2\2\u021c\u021b\3\2\2\2\u021d\u021e\3\2"+
		"\2\2\u021e\u021c\3\2\2\2\u021e\u021f\3\2\2\2\u021f\u0220\3\2\2\2\u0220"+
		"\u0221\b-\2\2\u0221Z\3\2\2\2\16\2dhnqw\177\u0111\u0206\u020b\u0218\u021e"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}