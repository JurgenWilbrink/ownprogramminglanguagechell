// Generated from C:/Users/jurge/Desktop/Java-programs/MyOwnLanguageChellMap/GitifiedChell/src\Chell.g4 by ANTLR 4.8
package nl.saxion.cos;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ChellParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ChellVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ChellParser#project}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProject(ChellParser.ProjectContext ctx);
	/**
	 * Visit a parse tree produced by {@link ChellParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatements(ChellParser.StatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ChellParser#method}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod(ChellParser.MethodContext ctx);
	/**
	 * Visit a parse tree produced by {@link ChellParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(ChellParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatDeclaration}
	 * labeled alternative in {@link ChellParser#setValueStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatDeclaration(ChellParser.StatDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatReDeclaration}
	 * labeled alternative in {@link ChellParser#assignmentStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatReDeclaration(ChellParser.StatReDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatDeclareVarName}
	 * labeled alternative in {@link ChellParser#declareVariableNameStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatDeclareVarName(ChellParser.StatDeclareVarNameContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatIf}
	 * labeled alternative in {@link ChellParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatIf(ChellParser.StatIfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatWhile}
	 * labeled alternative in {@link ChellParser#whileStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatWhile(ChellParser.StatWhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatOut}
	 * labeled alternative in {@link ChellParser#outputStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatOut(ChellParser.StatOutContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExIdentifier}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExIdentifier(ChellParser.ExIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExNegate}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExNegate(ChellParser.ExNegateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExParentHaakjes}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExParentHaakjes(ChellParser.ExParentHaakjesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExMethodCall}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExMethodCall(ChellParser.ExMethodCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExMulOp}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExMulOp(ChellParser.ExMulOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExStrLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExStrLiteral(ChellParser.ExStrLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExAddOp}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExAddOp(ChellParser.ExAddOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExBoolLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExBoolLiteral(ChellParser.ExBoolLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExEqualsE}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExEqualsE(ChellParser.ExEqualsEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExIntLiteral}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExIntLiteral(ChellParser.ExIntLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExInput}
	 * labeled alternative in {@link ChellParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExInput(ChellParser.ExInputContext ctx);
	/**
	 * Visit a parse tree produced by {@link ChellParser#equalsSigning}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualsSigning(ChellParser.EqualsSigningContext ctx);
	/**
	 * Visit a parse tree produced by {@link ChellParser#askForInputExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAskForInputExpression(ChellParser.AskForInputExpressionContext ctx);
}