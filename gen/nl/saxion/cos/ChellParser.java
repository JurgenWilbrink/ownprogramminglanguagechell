// Generated from C:/Users/jurge/Desktop/Java-programs/MyOwnLanguageChellMap/GitifiedChell/src\Chell.g4 by ANTLR 4.8
package nl.saxion.cos;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ChellParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BOOLEAN=1, INT=2, STRING=3, TYPENAME=4, BOOLEANNAME=5, INTNAME=6, STRINGNAME=7, 
		RETURN=8, EQUALS=9, NOTEQ=10, AND=11, OR=12, BT=13, LT=14, BTEQ=15, LTEQ=16, 
		SS=17, ME=18, PM=19, ML=20, SPACE=21, EQSIGN=22, PLUS=23, MINUS=24, MINUSWITHOUTSPACE=25, 
		COLON=26, PARENTBRACKET=27, ROUNDBRACKETOPEN=28, ROUNDBRACKETCLOSE=29, 
		COMMA=30, CREATEMETHOD=31, SHOULDREPLYWITH=32, SETPERSONALITYWITHSETTINGS=33, 
		PORTAL=34, NEXTPORTAL=35, NOWIWANTYOUGONE=36, TURRETKEEPSONSHOOTINGUNTIL=37, 
		IVEGOTANOTEHERE=38, PROGRAMSTART=39, PROGRAMEND=40, ASKFORSTRINGINPUT=41, 
		NAME=42, WS=43, COMMENT=44;
	public static final int
		RULE_project = 0, RULE_statements = 1, RULE_method = 2, RULE_statement = 3, 
		RULE_setValueStatement = 4, RULE_assignmentStatement = 5, RULE_declareVariableNameStatement = 6, 
		RULE_ifStatement = 7, RULE_whileStatement = 8, RULE_outputStatement = 9, 
		RULE_expression = 10, RULE_equalsSigning = 11, RULE_askForInputExpression = 12;
	private static String[] makeRuleNames() {
		return new String[] {
			"project", "statements", "method", "statement", "setValueStatement", 
			"assignmentStatement", "declareVariableNameStatement", "ifStatement", 
			"whileStatement", "outputStatement", "expression", "equalsSigning", "askForInputExpression"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, "'cake'", "'potatos'", "'notes'", "'chapter3 '", 
			"'is not a lie'", "'is a lie'", "'end'", "'ore'", "'higher level of'", 
			"'lower level of'", "'higher then or on level'", "'lower then or on level'", 
			"'?'", "'##'", null, "'*'", "' '", null, null, null, "'-'", "':'", "'|'", 
			"'('", "')'", null, "'create core'", "'should reply with'", "'set personnality with settings'", 
			"'portal'", "'next portal'", "'now i want you gone'", "'turret keeps on shooting until'", 
			"'Ive got a note here'", "'IM MAKING A NOTE HERE'", "'This was a triumph'", 
			"'Give me some notes'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "BOOLEAN", "INT", "STRING", "TYPENAME", "BOOLEANNAME", "INTNAME", 
			"STRINGNAME", "RETURN", "EQUALS", "NOTEQ", "AND", "OR", "BT", "LT", "BTEQ", 
			"LTEQ", "SS", "ME", "PM", "ML", "SPACE", "EQSIGN", "PLUS", "MINUS", "MINUSWITHOUTSPACE", 
			"COLON", "PARENTBRACKET", "ROUNDBRACKETOPEN", "ROUNDBRACKETCLOSE", "COMMA", 
			"CREATEMETHOD", "SHOULDREPLYWITH", "SETPERSONALITYWITHSETTINGS", "PORTAL", 
			"NEXTPORTAL", "NOWIWANTYOUGONE", "TURRETKEEPSONSHOOTINGUNTIL", "IVEGOTANOTEHERE", 
			"PROGRAMSTART", "PROGRAMEND", "ASKFORSTRINGINPUT", "NAME", "WS", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Chell.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ChellParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProjectContext extends ParserRuleContext {
		public TerminalNode PROGRAMSTART() { return getToken(ChellParser.PROGRAMSTART, 0); }
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public TerminalNode PROGRAMEND() { return getToken(ChellParser.PROGRAMEND, 0); }
		public List<MethodContext> method() {
			return getRuleContexts(MethodContext.class);
		}
		public MethodContext method(int i) {
			return getRuleContext(MethodContext.class,i);
		}
		public ProjectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_project; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterProject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitProject(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitProject(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProjectContext project() throws RecognitionException {
		ProjectContext _localctx = new ProjectContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_project);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			match(PROGRAMSTART);
			setState(30);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CREATEMETHOD) {
				{
				{
				setState(27);
				method();
				}
				}
				setState(32);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(33);
			statements();
			setState(34);
			match(PROGRAMEND);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementsContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<TerminalNode> SS() { return getTokens(ChellParser.SS); }
		public TerminalNode SS(int i) {
			return getToken(ChellParser.SS, i);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		StatementsContext _localctx = new StatementsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statements);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << STRING) | (1L << TYPENAME) | (1L << MINUSWITHOUTSPACE) | (1L << PARENTBRACKET) | (1L << ROUNDBRACKETOPEN) | (1L << PORTAL) | (1L << TURRETKEEPSONSHOOTINGUNTIL) | (1L << IVEGOTANOTEHERE) | (1L << ASKFORSTRINGINPUT) | (1L << NAME))) != 0)) {
				{
				{
				setState(36);
				statement();
				setState(37);
				match(SS);
				}
				}
				setState(43);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodContext extends ParserRuleContext {
		public TerminalNode CREATEMETHOD() { return getToken(ChellParser.CREATEMETHOD, 0); }
		public List<TerminalNode> SPACE() { return getTokens(ChellParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(ChellParser.SPACE, i);
		}
		public TerminalNode NAME() { return getToken(ChellParser.NAME, 0); }
		public TerminalNode COLON() { return getToken(ChellParser.COLON, 0); }
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public TerminalNode ME() { return getToken(ChellParser.ME, 0); }
		public List<TerminalNode> COMMA() { return getTokens(ChellParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(ChellParser.COMMA, i);
		}
		public TerminalNode SHOULDREPLYWITH() { return getToken(ChellParser.SHOULDREPLYWITH, 0); }
		public TerminalNode TYPENAME() { return getToken(ChellParser.TYPENAME, 0); }
		public TerminalNode SETPERSONALITYWITHSETTINGS() { return getToken(ChellParser.SETPERSONALITYWITHSETTINGS, 0); }
		public TerminalNode RETURN() { return getToken(ChellParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SS() { return getToken(ChellParser.SS, 0); }
		public List<DeclareVariableNameStatementContext> declareVariableNameStatement() {
			return getRuleContexts(DeclareVariableNameStatementContext.class);
		}
		public DeclareVariableNameStatementContext declareVariableNameStatement(int i) {
			return getRuleContext(DeclareVariableNameStatementContext.class,i);
		}
		public MethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitMethod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitMethod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodContext method() throws RecognitionException {
		MethodContext _localctx = new MethodContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_method);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44);
			match(CREATEMETHOD);
			setState(45);
			match(SPACE);
			setState(46);
			match(NAME);
			setState(51);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(47);
				match(COMMA);
				setState(48);
				match(SHOULDREPLYWITH);
				setState(49);
				match(SPACE);
				setState(50);
				match(TYPENAME);
				}
				break;
			}
			setState(64);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(53);
				match(COMMA);
				setState(54);
				match(SETPERSONALITYWITHSETTINGS);
				setState(55);
				match(SPACE);
				{
				setState(56);
				declareVariableNameStatement();
				}
				setState(61);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(57);
					match(COMMA);
					setState(58);
					declareVariableNameStatement();
					}
					}
					setState(63);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(66);
			match(COLON);
			setState(67);
			statements();
			setState(72);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(68);
				match(RETURN);
				setState(69);
				expression(0);
				setState(70);
				match(SS);
				}
			}

			setState(74);
			match(ME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public OutputStatementContext outputStatement() {
			return getRuleContext(OutputStatementContext.class,0);
		}
		public WhileStatementContext whileStatement() {
			return getRuleContext(WhileStatementContext.class,0);
		}
		public SetValueStatementContext setValueStatement() {
			return getRuleContext(SetValueStatementContext.class,0);
		}
		public AssignmentStatementContext assignmentStatement() {
			return getRuleContext(AssignmentStatementContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_statement);
		try {
			setState(82);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(76);
				ifStatement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(77);
				outputStatement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(78);
				whileStatement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(79);
				setValueStatement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(80);
				assignmentStatement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(81);
				expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetValueStatementContext extends ParserRuleContext {
		public SetValueStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setValueStatement; }
	 
		public SetValueStatementContext() { }
		public void copyFrom(SetValueStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StatDeclarationContext extends SetValueStatementContext {
		public TerminalNode TYPENAME() { return getToken(ChellParser.TYPENAME, 0); }
		public TerminalNode SPACE() { return getToken(ChellParser.SPACE, 0); }
		public TerminalNode NAME() { return getToken(ChellParser.NAME, 0); }
		public TerminalNode EQSIGN() { return getToken(ChellParser.EQSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatDeclarationContext(SetValueStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetValueStatementContext setValueStatement() throws RecognitionException {
		SetValueStatementContext _localctx = new SetValueStatementContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_setValueStatement);
		try {
			_localctx = new StatDeclarationContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			match(TYPENAME);
			setState(85);
			match(SPACE);
			setState(86);
			match(NAME);
			setState(87);
			match(EQSIGN);
			setState(88);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentStatementContext extends ParserRuleContext {
		public AssignmentStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentStatement; }
	 
		public AssignmentStatementContext() { }
		public void copyFrom(AssignmentStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StatReDeclarationContext extends AssignmentStatementContext {
		public TerminalNode NAME() { return getToken(ChellParser.NAME, 0); }
		public TerminalNode EQSIGN() { return getToken(ChellParser.EQSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatReDeclarationContext(AssignmentStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatReDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatReDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatReDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentStatementContext assignmentStatement() throws RecognitionException {
		AssignmentStatementContext _localctx = new AssignmentStatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_assignmentStatement);
		try {
			_localctx = new StatReDeclarationContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			match(NAME);
			setState(91);
			match(EQSIGN);
			setState(92);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclareVariableNameStatementContext extends ParserRuleContext {
		public DeclareVariableNameStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declareVariableNameStatement; }
	 
		public DeclareVariableNameStatementContext() { }
		public void copyFrom(DeclareVariableNameStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StatDeclareVarNameContext extends DeclareVariableNameStatementContext {
		public TerminalNode TYPENAME() { return getToken(ChellParser.TYPENAME, 0); }
		public TerminalNode SPACE() { return getToken(ChellParser.SPACE, 0); }
		public TerminalNode NAME() { return getToken(ChellParser.NAME, 0); }
		public StatDeclareVarNameContext(DeclareVariableNameStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatDeclareVarName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatDeclareVarName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatDeclareVarName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclareVariableNameStatementContext declareVariableNameStatement() throws RecognitionException {
		DeclareVariableNameStatementContext _localctx = new DeclareVariableNameStatementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_declareVariableNameStatement);
		try {
			_localctx = new StatDeclareVarNameContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(94);
			match(TYPENAME);
			setState(95);
			match(SPACE);
			setState(96);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
	 
		public IfStatementContext() { }
		public void copyFrom(IfStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StatIfContext extends IfStatementContext {
		public ExpressionContext exFirst;
		public StatementsContext statFirst;
		public ExpressionContext expression;
		public List<ExpressionContext> exInner = new ArrayList<ExpressionContext>();
		public StatementsContext statements;
		public List<StatementsContext> statInner = new ArrayList<StatementsContext>();
		public StatementsContext statLast;
		public TerminalNode PORTAL() { return getToken(ChellParser.PORTAL, 0); }
		public List<TerminalNode> SPACE() { return getTokens(ChellParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(ChellParser.SPACE, i);
		}
		public List<TerminalNode> COLON() { return getTokens(ChellParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(ChellParser.COLON, i);
		}
		public List<TerminalNode> ME() { return getTokens(ChellParser.ME); }
		public TerminalNode ME(int i) {
			return getToken(ChellParser.ME, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<StatementsContext> statements() {
			return getRuleContexts(StatementsContext.class);
		}
		public StatementsContext statements(int i) {
			return getRuleContext(StatementsContext.class,i);
		}
		public List<TerminalNode> NEXTPORTAL() { return getTokens(ChellParser.NEXTPORTAL); }
		public TerminalNode NEXTPORTAL(int i) {
			return getToken(ChellParser.NEXTPORTAL, i);
		}
		public TerminalNode NOWIWANTYOUGONE() { return getToken(ChellParser.NOWIWANTYOUGONE, 0); }
		public StatIfContext(IfStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatIf(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_ifStatement);
		int _la;
		try {
			int _alt;
			_localctx = new StatIfContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			match(PORTAL);
			setState(99);
			match(SPACE);
			setState(100);
			((StatIfContext)_localctx).exFirst = expression(0);
			setState(101);
			match(COLON);
			setState(102);
			((StatIfContext)_localctx).statFirst = statements();
			setState(103);
			match(ME);
			setState(114);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(104);
					match(SPACE);
					setState(105);
					match(NEXTPORTAL);
					setState(106);
					match(SPACE);
					setState(107);
					((StatIfContext)_localctx).expression = expression(0);
					((StatIfContext)_localctx).exInner.add(((StatIfContext)_localctx).expression);
					setState(108);
					match(COLON);
					setState(109);
					((StatIfContext)_localctx).statements = statements();
					((StatIfContext)_localctx).statInner.add(((StatIfContext)_localctx).statements);
					setState(110);
					match(ME);
					}
					} 
				}
				setState(116);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			setState(123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SPACE) {
				{
				setState(117);
				match(SPACE);
				setState(118);
				match(NOWIWANTYOUGONE);
				setState(119);
				match(COLON);
				setState(120);
				((StatIfContext)_localctx).statLast = statements();
				setState(121);
				match(ME);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatementContext extends ParserRuleContext {
		public WhileStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStatement; }
	 
		public WhileStatementContext() { }
		public void copyFrom(WhileStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StatWhileContext extends WhileStatementContext {
		public TerminalNode TURRETKEEPSONSHOOTINGUNTIL() { return getToken(ChellParser.TURRETKEEPSONSHOOTINGUNTIL, 0); }
		public TerminalNode SPACE() { return getToken(ChellParser.SPACE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode COLON() { return getToken(ChellParser.COLON, 0); }
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public TerminalNode ME() { return getToken(ChellParser.ME, 0); }
		public StatWhileContext(WhileStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatWhile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileStatementContext whileStatement() throws RecognitionException {
		WhileStatementContext _localctx = new WhileStatementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_whileStatement);
		try {
			_localctx = new StatWhileContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(TURRETKEEPSONSHOOTINGUNTIL);
			setState(126);
			match(SPACE);
			setState(127);
			expression(0);
			setState(128);
			match(COLON);
			setState(129);
			statements();
			setState(130);
			match(ME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OutputStatementContext extends ParserRuleContext {
		public OutputStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_outputStatement; }
	 
		public OutputStatementContext() { }
		public void copyFrom(OutputStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StatOutContext extends OutputStatementContext {
		public TerminalNode IVEGOTANOTEHERE() { return getToken(ChellParser.IVEGOTANOTEHERE, 0); }
		public TerminalNode COLON() { return getToken(ChellParser.COLON, 0); }
		public TerminalNode SPACE() { return getToken(ChellParser.SPACE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatOutContext(OutputStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterStatOut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitStatOut(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitStatOut(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OutputStatementContext outputStatement() throws RecognitionException {
		OutputStatementContext _localctx = new OutputStatementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_outputStatement);
		try {
			_localctx = new StatOutContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(132);
			match(IVEGOTANOTEHERE);
			setState(133);
			match(COLON);
			setState(134);
			match(SPACE);
			setState(135);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExIdentifierContext extends ExpressionContext {
		public TerminalNode NAME() { return getToken(ChellParser.NAME, 0); }
		public ExIdentifierContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExNegateContext extends ExpressionContext {
		public TerminalNode MINUSWITHOUTSPACE() { return getToken(ChellParser.MINUSWITHOUTSPACE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExNegateContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExNegate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExNegate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExNegate(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExParentHaakjesContext extends ExpressionContext {
		public List<TerminalNode> PARENTBRACKET() { return getTokens(ChellParser.PARENTBRACKET); }
		public TerminalNode PARENTBRACKET(int i) {
			return getToken(ChellParser.PARENTBRACKET, i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExParentHaakjesContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExParentHaakjes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExParentHaakjes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExParentHaakjes(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExMethodCallContext extends ExpressionContext {
		public TerminalNode NAME() { return getToken(ChellParser.NAME, 0); }
		public TerminalNode ROUNDBRACKETOPEN() { return getToken(ChellParser.ROUNDBRACKETOPEN, 0); }
		public TerminalNode ROUNDBRACKETCLOSE() { return getToken(ChellParser.ROUNDBRACKETCLOSE, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(ChellParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(ChellParser.COMMA, i);
		}
		public ExMethodCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExMethodCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExMethodCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExMethodCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExMulOpContext extends ExpressionContext {
		public ExpressionContext left;
		public ExpressionContext right;
		public List<TerminalNode> SPACE() { return getTokens(ChellParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(ChellParser.SPACE, i);
		}
		public TerminalNode ML() { return getToken(ChellParser.ML, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ExMulOpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExMulOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExMulOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExMulOp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExStrLiteralContext extends ExpressionContext {
		public TerminalNode STRING() { return getToken(ChellParser.STRING, 0); }
		public ExStrLiteralContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExStrLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExStrLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExStrLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExAddOpContext extends ExpressionContext {
		public ExpressionContext left;
		public Token op;
		public ExpressionContext right;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PM() { return getToken(ChellParser.PM, 0); }
		public ExAddOpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExAddOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExAddOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExAddOp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExBoolLiteralContext extends ExpressionContext {
		public TerminalNode BOOLEAN() { return getToken(ChellParser.BOOLEAN, 0); }
		public ExBoolLiteralContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExBoolLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExBoolLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExBoolLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExEqualsEContext extends ExpressionContext {
		public ExpressionContext left;
		public EqualsSigningContext op;
		public ExpressionContext right;
		public TerminalNode ROUNDBRACKETOPEN() { return getToken(ChellParser.ROUNDBRACKETOPEN, 0); }
		public List<TerminalNode> SPACE() { return getTokens(ChellParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(ChellParser.SPACE, i);
		}
		public TerminalNode ROUNDBRACKETCLOSE() { return getToken(ChellParser.ROUNDBRACKETCLOSE, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public EqualsSigningContext equalsSigning() {
			return getRuleContext(EqualsSigningContext.class,0);
		}
		public ExEqualsEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExEqualsE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExEqualsE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExEqualsE(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExIntLiteralContext extends ExpressionContext {
		public TerminalNode INT() { return getToken(ChellParser.INT, 0); }
		public ExIntLiteralContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExIntLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExIntLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExIntLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExInputContext extends ExpressionContext {
		public AskForInputExpressionContext askForInputExpression() {
			return getRuleContext(AskForInputExpressionContext.class,0);
		}
		public ExInputContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterExInput(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitExInput(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitExInput(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				_localctx = new ExParentHaakjesContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(138);
				match(PARENTBRACKET);
				setState(139);
				expression(0);
				setState(140);
				match(PARENTBRACKET);
				}
				break;
			case 2:
				{
				_localctx = new ExMethodCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(142);
				match(NAME);
				setState(143);
				match(ROUNDBRACKETOPEN);
				setState(152);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << STRING) | (1L << MINUSWITHOUTSPACE) | (1L << PARENTBRACKET) | (1L << ROUNDBRACKETOPEN) | (1L << ASKFORSTRINGINPUT) | (1L << NAME))) != 0)) {
					{
					setState(144);
					expression(0);
					setState(149);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(145);
						match(COMMA);
						setState(146);
						expression(0);
						}
						}
						setState(151);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(154);
				match(ROUNDBRACKETCLOSE);
				}
				break;
			case 3:
				{
				_localctx = new ExIdentifierContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(155);
				match(NAME);
				}
				break;
			case 4:
				{
				_localctx = new ExStrLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(156);
				match(STRING);
				}
				break;
			case 5:
				{
				_localctx = new ExNegateContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(157);
				match(MINUSWITHOUTSPACE);
				setState(158);
				expression(7);
				}
				break;
			case 6:
				{
				_localctx = new ExEqualsEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(159);
				match(ROUNDBRACKETOPEN);
				setState(160);
				((ExEqualsEContext)_localctx).left = expression(0);
				setState(161);
				match(SPACE);
				setState(162);
				((ExEqualsEContext)_localctx).op = equalsSigning();
				setState(163);
				match(SPACE);
				setState(164);
				((ExEqualsEContext)_localctx).right = expression(0);
				setState(165);
				match(ROUNDBRACKETCLOSE);
				}
				break;
			case 7:
				{
				_localctx = new ExBoolLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(167);
				match(BOOLEAN);
				}
				break;
			case 8:
				{
				_localctx = new ExIntLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(168);
				match(INT);
				}
				break;
			case 9:
				{
				_localctx = new ExInputContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(169);
				askForInputExpression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(182);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(180);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						_localctx = new ExAddOpContext(new ExpressionContext(_parentctx, _parentState));
						((ExAddOpContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(172);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(173);
						((ExAddOpContext)_localctx).op = match(PM);
						setState(174);
						((ExAddOpContext)_localctx).right = expression(3);
						}
						break;
					case 2:
						{
						_localctx = new ExMulOpContext(new ExpressionContext(_parentctx, _parentState));
						((ExMulOpContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(175);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(176);
						match(SPACE);
						setState(177);
						match(ML);
						setState(178);
						match(SPACE);
						setState(179);
						((ExMulOpContext)_localctx).right = expression(2);
						}
						break;
					}
					} 
				}
				setState(184);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualsSigningContext extends ParserRuleContext {
		public TerminalNode EQUALS() { return getToken(ChellParser.EQUALS, 0); }
		public TerminalNode NOTEQ() { return getToken(ChellParser.NOTEQ, 0); }
		public TerminalNode AND() { return getToken(ChellParser.AND, 0); }
		public TerminalNode OR() { return getToken(ChellParser.OR, 0); }
		public TerminalNode BT() { return getToken(ChellParser.BT, 0); }
		public TerminalNode LT() { return getToken(ChellParser.LT, 0); }
		public TerminalNode BTEQ() { return getToken(ChellParser.BTEQ, 0); }
		public TerminalNode LTEQ() { return getToken(ChellParser.LTEQ, 0); }
		public EqualsSigningContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalsSigning; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterEqualsSigning(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitEqualsSigning(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitEqualsSigning(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualsSigningContext equalsSigning() throws RecognitionException {
		EqualsSigningContext _localctx = new EqualsSigningContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_equalsSigning);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUALS) | (1L << NOTEQ) | (1L << AND) | (1L << OR) | (1L << BT) | (1L << LT) | (1L << BTEQ) | (1L << LTEQ))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AskForInputExpressionContext extends ParserRuleContext {
		public TerminalNode ASKFORSTRINGINPUT() { return getToken(ChellParser.ASKFORSTRINGINPUT, 0); }
		public AskForInputExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_askForInputExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).enterAskForInputExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChellListener ) ((ChellListener)listener).exitAskForInputExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ChellVisitor ) return ((ChellVisitor<? extends T>)visitor).visitAskForInputExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AskForInputExpressionContext askForInputExpression() throws RecognitionException {
		AskForInputExpressionContext _localctx = new AskForInputExpressionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_askForInputExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			match(ASKFORSTRINGINPUT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 10:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3.\u00c0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\7\2\37\n\2\f\2\16\2\"\13\2\3\2\3"+
		"\2\3\2\3\3\3\3\3\3\7\3*\n\3\f\3\16\3-\13\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\5\4\66\n\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4>\n\4\f\4\16\4A\13\4\5\4C\n\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\5\4K\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5U\n"+
		"\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\ts\n\t\f\t\16\tv\13\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\5\t~\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13"+
		"\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u0096\n\f"+
		"\f\f\16\f\u0099\13\f\5\f\u009b\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00ad\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\7\f\u00b7\n\f\f\f\16\f\u00ba\13\f\3\r\3\r\3\16\3\16\3\16\2\3\26\17"+
		"\2\4\6\b\n\f\16\20\22\24\26\30\32\2\3\3\2\13\22\2\u00cb\2\34\3\2\2\2\4"+
		"+\3\2\2\2\6.\3\2\2\2\bT\3\2\2\2\nV\3\2\2\2\f\\\3\2\2\2\16`\3\2\2\2\20"+
		"d\3\2\2\2\22\177\3\2\2\2\24\u0086\3\2\2\2\26\u00ac\3\2\2\2\30\u00bb\3"+
		"\2\2\2\32\u00bd\3\2\2\2\34 \7)\2\2\35\37\5\6\4\2\36\35\3\2\2\2\37\"\3"+
		"\2\2\2 \36\3\2\2\2 !\3\2\2\2!#\3\2\2\2\" \3\2\2\2#$\5\4\3\2$%\7*\2\2%"+
		"\3\3\2\2\2&\'\5\b\5\2\'(\7\23\2\2(*\3\2\2\2)&\3\2\2\2*-\3\2\2\2+)\3\2"+
		"\2\2+,\3\2\2\2,\5\3\2\2\2-+\3\2\2\2./\7!\2\2/\60\7\27\2\2\60\65\7,\2\2"+
		"\61\62\7 \2\2\62\63\7\"\2\2\63\64\7\27\2\2\64\66\7\6\2\2\65\61\3\2\2\2"+
		"\65\66\3\2\2\2\66B\3\2\2\2\678\7 \2\289\7#\2\29:\7\27\2\2:?\5\16\b\2;"+
		"<\7 \2\2<>\5\16\b\2=;\3\2\2\2>A\3\2\2\2?=\3\2\2\2?@\3\2\2\2@C\3\2\2\2"+
		"A?\3\2\2\2B\67\3\2\2\2BC\3\2\2\2CD\3\2\2\2DE\7\34\2\2EJ\5\4\3\2FG\7\n"+
		"\2\2GH\5\26\f\2HI\7\23\2\2IK\3\2\2\2JF\3\2\2\2JK\3\2\2\2KL\3\2\2\2LM\7"+
		"\24\2\2M\7\3\2\2\2NU\5\20\t\2OU\5\24\13\2PU\5\22\n\2QU\5\n\6\2RU\5\f\7"+
		"\2SU\5\26\f\2TN\3\2\2\2TO\3\2\2\2TP\3\2\2\2TQ\3\2\2\2TR\3\2\2\2TS\3\2"+
		"\2\2U\t\3\2\2\2VW\7\6\2\2WX\7\27\2\2XY\7,\2\2YZ\7\30\2\2Z[\5\26\f\2[\13"+
		"\3\2\2\2\\]\7,\2\2]^\7\30\2\2^_\5\26\f\2_\r\3\2\2\2`a\7\6\2\2ab\7\27\2"+
		"\2bc\7,\2\2c\17\3\2\2\2de\7$\2\2ef\7\27\2\2fg\5\26\f\2gh\7\34\2\2hi\5"+
		"\4\3\2it\7\24\2\2jk\7\27\2\2kl\7%\2\2lm\7\27\2\2mn\5\26\f\2no\7\34\2\2"+
		"op\5\4\3\2pq\7\24\2\2qs\3\2\2\2rj\3\2\2\2sv\3\2\2\2tr\3\2\2\2tu\3\2\2"+
		"\2u}\3\2\2\2vt\3\2\2\2wx\7\27\2\2xy\7&\2\2yz\7\34\2\2z{\5\4\3\2{|\7\24"+
		"\2\2|~\3\2\2\2}w\3\2\2\2}~\3\2\2\2~\21\3\2\2\2\177\u0080\7\'\2\2\u0080"+
		"\u0081\7\27\2\2\u0081\u0082\5\26\f\2\u0082\u0083\7\34\2\2\u0083\u0084"+
		"\5\4\3\2\u0084\u0085\7\24\2\2\u0085\23\3\2\2\2\u0086\u0087\7(\2\2\u0087"+
		"\u0088\7\34\2\2\u0088\u0089\7\27\2\2\u0089\u008a\5\26\f\2\u008a\25\3\2"+
		"\2\2\u008b\u008c\b\f\1\2\u008c\u008d\7\35\2\2\u008d\u008e\5\26\f\2\u008e"+
		"\u008f\7\35\2\2\u008f\u00ad\3\2\2\2\u0090\u0091\7,\2\2\u0091\u009a\7\36"+
		"\2\2\u0092\u0097\5\26\f\2\u0093\u0094\7 \2\2\u0094\u0096\5\26\f\2\u0095"+
		"\u0093\3\2\2\2\u0096\u0099\3\2\2\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2"+
		"\2\2\u0098\u009b\3\2\2\2\u0099\u0097\3\2\2\2\u009a\u0092\3\2\2\2\u009a"+
		"\u009b\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u00ad\7\37\2\2\u009d\u00ad\7"+
		",\2\2\u009e\u00ad\7\5\2\2\u009f\u00a0\7\33\2\2\u00a0\u00ad\5\26\f\t\u00a1"+
		"\u00a2\7\36\2\2\u00a2\u00a3\5\26\f\2\u00a3\u00a4\7\27\2\2\u00a4\u00a5"+
		"\5\30\r\2\u00a5\u00a6\7\27\2\2\u00a6\u00a7\5\26\f\2\u00a7\u00a8\7\37\2"+
		"\2\u00a8\u00ad\3\2\2\2\u00a9\u00ad\7\3\2\2\u00aa\u00ad\7\4\2\2\u00ab\u00ad"+
		"\5\32\16\2\u00ac\u008b\3\2\2\2\u00ac\u0090\3\2\2\2\u00ac\u009d\3\2\2\2"+
		"\u00ac\u009e\3\2\2\2\u00ac\u009f\3\2\2\2\u00ac\u00a1\3\2\2\2\u00ac\u00a9"+
		"\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ac\u00ab\3\2\2\2\u00ad\u00b8\3\2\2\2\u00ae"+
		"\u00af\f\4\2\2\u00af\u00b0\7\25\2\2\u00b0\u00b7\5\26\f\5\u00b1\u00b2\f"+
		"\3\2\2\u00b2\u00b3\7\27\2\2\u00b3\u00b4\7\26\2\2\u00b4\u00b5\7\27\2\2"+
		"\u00b5\u00b7\5\26\f\4\u00b6\u00ae\3\2\2\2\u00b6\u00b1\3\2\2\2\u00b7\u00ba"+
		"\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\27\3\2\2\2\u00ba"+
		"\u00b8\3\2\2\2\u00bb\u00bc\t\2\2\2\u00bc\31\3\2\2\2\u00bd\u00be\7+\2\2"+
		"\u00be\33\3\2\2\2\20 +\65?BJTt}\u0097\u009a\u00ac\u00b6\u00b8";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}