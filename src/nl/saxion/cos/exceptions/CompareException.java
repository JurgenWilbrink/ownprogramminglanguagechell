package nl.saxion.cos.exceptions;

public class CompareException extends RuntimeException {

    public CompareException(String msg) {
        super(msg);
    }
}
