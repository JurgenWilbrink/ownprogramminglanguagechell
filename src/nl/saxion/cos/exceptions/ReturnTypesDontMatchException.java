package nl.saxion.cos.exceptions;

public class ReturnTypesDontMatchException extends RuntimeException {
    public ReturnTypesDontMatchException(String msg) {
        super(msg);
    }
}
