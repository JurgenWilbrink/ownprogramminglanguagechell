package nl.saxion.cos.exceptions;

/**
 * This error should be thrown when an error is found by the Checker.
 */
public class CompilerException extends RuntimeException {

    // the reason for this String is, its a Quote from the game.
    private static final String gLADoSOut = "THIS... STATEMENT... IS... FALSE..." +
            "\nDon't think about it!..." +
            "\nDon't think about it!..." +
            "\nDon't think about it!..." +
            "\nAn Error has been found in the Checker: ";

    public CompilerException(String msg) {
        super(gLADoSOut + msg);
    }
}