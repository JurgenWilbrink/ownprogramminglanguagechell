package nl.saxion.cos.model;

import java.util.List;

public class SymbolMethod extends Symbol {

    private DataType returnType;
    private List<DataType> paramTypes;

    protected SymbolMethod(String name, DataType returnType, List<DataType> paramTypes) {
        super(name);
        this.returnType = returnType;
        this.paramTypes = paramTypes;
    }

    public DataType getReturnType() {
        return returnType;
    }

    public List<DataType> getParamTypes() {
        return paramTypes;
    }

    @Override
    public String toString() {
        return "name: " + this.getName() + " returnType: " + this.returnType;
    }
}
