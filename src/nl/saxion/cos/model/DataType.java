package nl.saxion.cos.model;

public enum DataType {
	BOOLEAN,  //cake
	INT,      //potatos
	STRING    //notes
}
