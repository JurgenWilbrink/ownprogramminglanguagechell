package nl.saxion.cos.model;


//abstact symbol class
// 2 subclassen 1 voor symbolvaraibles
// 1 voor symbolmethods.

public abstract class Symbol {

    private final String name;

    protected Symbol(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
