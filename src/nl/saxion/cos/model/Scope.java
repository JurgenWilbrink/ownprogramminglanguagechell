package nl.saxion.cos.model;

import nl.saxion.cos.exceptions.CompilerException;

import java.util.HashMap;
import java.util.List;

public class Scope {
    private HashMap<String, Symbol> symbols;
    private final Scope parentScope;
    private int nextSlot;

    public Scope(Scope parentScope) {
        this.parentScope = parentScope;
        this.symbols = new HashMap<>();
        this.nextSlot = parentScope == null ? 0 : parentScope.nextSlot;
    }

    public SymbolVariable declareVariable(String name, DataType type) {

        if (symbols.containsKey(name)){
            throw new CompilerException("duplicate variable / method name already exists (name: " + name + ")");
        }

        SymbolVariable symbol = new SymbolVariable(name, type, nextSlot++);
        symbols.put(name, symbol);
        return symbol;
    }

    public SymbolMethod declareMethod(String name, DataType returnType, List<DataType> paramTypes){



        if (symbols.containsKey(name)){
            throw new CompilerException("duplicate variable / method name already exists (name: " + name + ")");
        }

        SymbolMethod symbol = new SymbolMethod(name, returnType, paramTypes);
        symbols.put(name, symbol);
        return symbol;
    }

    public Symbol lookupVariable(String name){
        Symbol symbol = symbols.get(name);
        if (symbol == null && parentScope != null) {
            symbol = parentScope.lookupVariable(name);
        }
        return symbol;
    }

    public Scope openScope(){
        return new Scope(this);
    }

    public Scope closeScope(){
        return parentScope;
    }

}
