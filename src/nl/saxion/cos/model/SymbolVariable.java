package nl.saxion.cos.model;

public class SymbolVariable extends Symbol {

    private DataType type;
    private int localSlot;

    public SymbolVariable(String name, DataType type, int localSlot) {
        super(name);
        this.type = type;
        this.localSlot = localSlot;

    }

    public DataType getType() {
        return type;
    }

    public int getLocalSlot() {
        return localSlot;
    }

    @Override
    public String toString() {
        return "name: " + this.getName() + " datatype: " + this.type + " localslot: " + this.localSlot;
    }
}
