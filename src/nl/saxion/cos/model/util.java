package nl.saxion.cos.model;

public class util {

    public static String getOpposite(String inputOp){
        switch (inputOp) {
            case "lower level of" :             return "ge"; // <
            case "lower then or on level" :     return "gt"; // <=
            case "higher level of" :            return "le"; // >
            case "higher then or on level" :    return "lt"; // >=
            case "is not a lie" :               return "ne"; // ==
            case "is a lie" :                   return "eq"; // !=
            //---------------------------------------------------- special cases
            case "ore" :                        return "ore"; // ||
            case "end" :                        return "end"; // &&
        }
        return null;
    }

    public static DataType getTypeFromString(String inputType){
        if (inputType.contains("potatos")){
            return DataType.INT;
        } else if (inputType.contains("cake")){
            return DataType.BOOLEAN;
        } else if (inputType.contains("notes")){
            return DataType.STRING;
        } else {
            return null;
        }
    }

    public static void test(String classFileName, String function, String actualData){
        System.out.println("<<TEST>> - Class: " + classFileName + ", Function: " + function + ", Data: " + actualData);
    }

    public static void genconfirm(String part){
        System.out.println("<<GenConfirm>> - " + part);
    }
}
