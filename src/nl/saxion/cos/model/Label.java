package nl.saxion.cos.model;

public class Label {

    private static int labelNr = 0;

    public static String newLabel(){
        return "L" + labelNr++;
    }

    public static String placeLabel(String label){
        return label + ":";
    }

    public static String goTo(String label){
        return "goto " + label;
    }

}
