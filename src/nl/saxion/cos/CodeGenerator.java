package nl.saxion.cos;

import nl.saxion.cos.exceptions.CompilerException;
import nl.saxion.cos.model.*;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import static nl.saxion.cos.model.Label.*;

public class CodeGenerator extends ChellBaseVisitor<Void> {
    private ParseTreeProperty<Symbol> symbols;
    private ParseTreeProperty<DataType> types;
    private JasminBytecode jasminCode;

    public CodeGenerator(ParseTreeProperty<Symbol> symbols, ParseTreeProperty<DataType> types, JasminBytecode jasminCode) {
        this.symbols = symbols;
        this.types = types;
        this.jasminCode = jasminCode;
    }

    @Override
    public Void visitStatements(ChellParser.StatementsContext ctx) {

        for (var stat : ctx.statement()) {
            visit(stat);
        }

        return null;
    }


    @Override //TODO
    public Void visitEqualsSigning(ChellParser.EqualsSigningContext ctx) {
        return super.visitEqualsSigning(ctx);
    }

    @Override
    public Void visitProject(ChellParser.ProjectContext ctx) {

        jasminCode.add(".bytecode 52.0");
        jasminCode.add(".source " + jasminCode.getClassName() + ".java");

        jasminCode.add(".class public " + jasminCode.getClassName());
        jasminCode.add(".super java/lang/Object");
        jasminCode.add("");

        jasminCode.add(".method public static main([Ljava/lang/String;)V");
        jasminCode.add(".limit stack 99");
        jasminCode.add(".limit locals 99");
        jasminCode.add("");

        visit(ctx.statements());

        jasminCode.add("");
        jasminCode.add("return");
        jasminCode.add(".end method");
        jasminCode.add("");
        jasminCode.add("");

        for (var m : ctx.method()) {
            visit(m);
        }

        return null;
    }

    @Override
    public Void visitMethod(ChellParser.MethodContext ctx) {

        //loop door formals heen kijken welke type het is. voeg de goede waarde toe tussen de haakjes

        String paramLetters = "";

        for (int i = 0; i < ctx.declareVariableNameStatement().size(); i++) {
            DataType type = util.getTypeFromString(ctx.declareVariableNameStatement().get(i).getText());

            if (type == DataType.INT) {
                paramLetters += "I";
            } else if (type == DataType.BOOLEAN) {
                paramLetters += "Z";
            } else if (type == DataType.STRING) {
                paramLetters += "Ljava/lang/String;";
            }
        }

        String returnLetter = "";
        String returnCommand = "";

        if (ctx.TYPENAME() == null){
            returnLetter = "V";
            returnCommand = "return";
        } else {
            DataType returnType = util.getTypeFromString(ctx.TYPENAME().getText());

            if (returnType == DataType.INT) {
                returnLetter = "I";
                returnCommand = "ireturn";
            } else if (returnType == DataType.BOOLEAN) {
                returnLetter = "Z";
                returnCommand = "ireturn";
            } else if (returnType == DataType.STRING) {
                returnLetter = "Ljava/lang/String;";
                returnCommand = "areturn";
            }
        }

        jasminCode.add(".method public static " + ctx.NAME() + "(" + paramLetters + ")" + returnLetter);
        jasminCode.add(".limit stack 99");
        jasminCode.add(".limit locals 99");

        jasminCode.add("");

        visit(ctx.statements());

        //if returntype is integer for example int moet als laatste op de stack

        if (ctx.expression() != null) {
            visit(ctx.expression());
        }

        jasminCode.add("");
        jasminCode.add("" + returnCommand);
        jasminCode.add(".end method");
        jasminCode.add("");
        jasminCode.add("");
        return null;
    }

    //TODO dubble code ?
    @Override
    public Void visitStatDeclaration(ChellParser.StatDeclarationContext ctx) {
        visit(ctx.expression());
        SymbolVariable symbol = (SymbolVariable) symbols.get(ctx);

        if (symbol.getType() == DataType.INT || symbol.getType() == DataType.BOOLEAN) {
            jasminCode.add("istore " + symbol.getLocalSlot());
        } else {
            jasminCode.add("astore " + symbol.getLocalSlot());
        }

        return null;
    }

    @Override
    public Void visitStatReDeclaration(ChellParser.StatReDeclarationContext ctx) {
        visit(ctx.expression());
        //DataType type = types.get(ctx);
        SymbolVariable symbol = (SymbolVariable) symbols.get(ctx);

        DataType type = symbol.getType();

        if (type == DataType.INT || type == DataType.BOOLEAN) {
            jasminCode.add("istore " + symbol.getLocalSlot());
        } else {
            jasminCode.add("astore " + symbol.getLocalSlot());
        }

        return null;
    }

    @Override
    public Void visitExParentHaakjes(ChellParser.ExParentHaakjesContext ctx) {
        visit(ctx.expression());
        return null;
    }

    @Override
    public Void visitExNegate(ChellParser.ExNegateContext ctx) {
        visit(ctx.expression());
        jasminCode.add("ineg");

        return null;
    }

    @Override
    public Void visitStatIf(ChellParser.StatIfContext ctx) {

        var firstEnd = newLabel();
        var endEndLabel = newLabel();

        visit(ctx.exFirst);

        jasminCode.add("ifeq " + firstEnd);

        visit(ctx.statFirst);

        jasminCode.add(goTo(endEndLabel));

        jasminCode.add(placeLabel(firstEnd));

        for (int i = 0; i < ctx.exInner.size(); i++) {
            var innerEndLabel = newLabel() + "I" + i;

            visit(ctx.exInner.get(i));

            jasminCode.add("ifeq " + innerEndLabel);

            visit(ctx.statInner.get(i));

            jasminCode.add(goTo(endEndLabel));

            jasminCode.add(placeLabel(innerEndLabel));
        }

        if (ctx.statLast != null) {
            visit(ctx.statLast);
        }


        jasminCode.add(placeLabel(endEndLabel));

        return null;
    }

    @Override
    public Void visitStatWhile(ChellParser.StatWhileContext ctx) {

        var startLabel = newLabel();
        var endLabel = newLabel();

        jasminCode.add(placeLabel(startLabel));

        visit(ctx.expression());

        jasminCode.add("ifeq " + endLabel);

        visit(ctx.statements());

        jasminCode.add(goTo(startLabel));
        jasminCode.add(placeLabel(endLabel));

        return null;
    }

    @Override
    public Void visitStatOut(ChellParser.StatOutContext ctx) {

        var expression = ctx.expression();

        jasminCode.add("getstatic java/lang/System/out Ljava/io/PrintStream;");

        visit(expression);

        if (types.get(expression) == DataType.INT) {
            jasminCode.add("invokevirtual java/io/PrintStream/println(I)V");
        } else if (types.get(expression) == DataType.BOOLEAN) {
            jasminCode.add("invokevirtual java/io/PrintStream/println(Z)V");
        } else if (types.get(expression) == DataType.STRING) {
            jasminCode.add("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        } else {
            throw new CompilerException("statOut Expression does not have a type!");
        }

        return null;
    }

    @Override
    public Void visitExStrLiteral(ChellParser.ExStrLiteralContext ctx) {
        jasminCode.add("ldc " + ctx.STRING().getText());
        return null;
    }

    @Override
    public Void visitExBoolLiteral(ChellParser.ExBoolLiteralContext ctx) {

        if (ctx.BOOLEAN().getText().equals("true")) {
            jasminCode.add("ldc 1");
        } else if (ctx.BOOLEAN().getText().equals("false")) {
            jasminCode.add("ldc 0");
        } else {
            jasminCode.add("ldc null");
        }

        return null;
    }

    @Override
    public Void visitExIntLiteral(ChellParser.ExIntLiteralContext ctx) {
        jasminCode.add("ldc " + ctx.INT().getText());
        return null;
    }

    @Override //TODO
    public Void visitExIdentifier(ChellParser.ExIdentifierContext ctx) {

        SymbolVariable symbolvar = (SymbolVariable) symbols.get(ctx);

        DataType type = symbolvar.getType();
        int localSlot = symbolvar.getLocalSlot();

        if (type == DataType.INT || type == DataType.BOOLEAN) {
            jasminCode.add("iload " + localSlot);
        } else if (type == DataType.STRING) {
            jasminCode.add("aload " + localSlot);
        } else {
            throw new CompilerException("Type for variable symbol doenst match any. (" + type + ")");
        }

        return null;
    }

    @Override
    public Void visitExMethodCall(ChellParser.ExMethodCallContext ctx) {

        SymbolMethod method = (SymbolMethod) symbols.get(ctx);

        String methodName = method.getName();

        StringBuilder params = new StringBuilder();
        
        if (ctx.expression() != null){
            for (DataType x : method.getParamTypes()) {
                if (x == DataType.STRING){
                    params.append("Ljava/lang/String;");
                } else if (x == DataType.BOOLEAN){
                    params.append("Z");
                } else if (x == DataType.INT){
                    params.append("I");
                } else {
                    throw new CompilerException("MethodCall, type not correct (" + x + ")");
                }
            }

            for (int i = 0; i < ctx.expression().size(); i++) {
                visit(ctx.expression().get(i));
            }

        }

        String returnType = "V";

        DataType type = method.getReturnType();

        if (type == DataType.STRING){
            returnType = "Ljava/lang/String;";
        } else if (type == DataType.INT){
            returnType = "I";
        } else if (type == DataType.BOOLEAN){
            returnType = "Z";
        }

        jasminCode.add("invokestatic " + jasminCode.getClassName() + "/" + methodName + "(" + params + ")" + returnType);

        return null;
    }

    @Override
    public Void visitExMulOp(ChellParser.ExMulOpContext ctx) {
        visit(ctx.left);
        visit(ctx.right);
        jasminCode.add("imul");
        return null;
    }

    @Override
    public Void visitExAddOp(ChellParser.ExAddOpContext ctx) {
        visit(ctx.left);
        visit(ctx.right);

        if (ctx.op.getText().equals(" - ")) {
            jasminCode.add("isub");
        } else if (ctx.op.getText().equals(" + ")) {
            jasminCode.add("iadd");
        } else {
            //this shouldnt be happening
            assert false;
        }

        return null;
    }

    @Override
    public Void visitExInput(ChellParser.ExInputContext ctx) {

        jasminCode.add("invokestatic java/lang/System/console()Ljava/io/Console;");
        jasminCode.add("invokevirtual java/io/Console/readLine()Ljava/lang/String;");

        // De originele oplossing. Niet mee verder gegaan vervangen door het hierboven.
//        String inputAskString = "";
//        String hasNextX = "hasNextInt()";
//        String nextX = "nextInt()I";
//
//        if (ctx.getText().equals("Is the cake a lie")) { //boolean
//            inputAskString = "Input a boolean: ";
//            hasNextX = "hasNextBoolean()";
//            nextX = "nextBoolean()Z";
//        } else if (ctx.getText().equals("Give me some notes")) { //String
//            inputAskString = "Input a String: ";
//            hasNextX = "hasNextLine()";
//            nextX = "nextLine()Ljava/lang/String;";
//        } else if (ctx.getText().equals("Set the Appature")) { //int
//            inputAskString = "Input a number: ";
//            hasNextX = "hasNextInt()";
//            nextX = "nextInt()I";
//        }
//
//        String retry = newLabel();
//        String done = newLabel();
//
//        jasminCode.add(placeLabel(retry));
//
//        jasminCode.add("getstatic java/lang/System/out Ljava/io/PrintStream;");
//        jasminCode.add("ldc \"" + inputAskString + "\"");
//        jasminCode.add("invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V");
//
//
//
//        jasminCode.add("dup");
//        jasminCode.add("invokevirtual java/util/Scanner/" + hasNextX + "Z");
//
//        jasminCode.add("ifne " + done);
//
//        jasminCode.add("dup");
//        jasminCode.add("invokevirtual java/util/Scanner/nextLine()Ljava/lang/String;");
//        jasminCode.add("pop");
//        jasminCode.add(goTo(retry));
//        jasminCode.add(placeLabel(done));
//        jasminCode.add("invokevirtual java/util/Scanner/" + nextX);

        return null;
    }

    @Override
    public Void visitExEqualsE(ChellParser.ExEqualsEContext ctx) {
        visit(ctx.left);
        visit(ctx.right);

        var falseLabel = newLabel();
        var doneLabel = newLabel();

        String opposite = util.getOpposite(ctx.op.getText());

        if (opposite == null) {
            throw new CompilerException("something went very wrong VisitExEqualsE Opposite is null");
        }

        if (opposite.equals("ore")) {
            jasminCode.add("ior");
        } else if (opposite.equals("end")) {
            jasminCode.add("iand");
        } else {
            jasminCode.add("if_icmp" + opposite + " " + falseLabel);
        }

        jasminCode.add("ldc 1"); //true

        jasminCode.add(goTo(doneLabel));
        jasminCode.add(placeLabel(falseLabel));

        jasminCode.add("ldc 0"); //false

        jasminCode.add(placeLabel(doneLabel));

        return null;
    }
}



