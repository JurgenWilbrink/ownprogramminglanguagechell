package nl.saxion.cos;

import nl.saxion.cos.exceptions.CompareException;
import nl.saxion.cos.exceptions.CompilerException;
import nl.saxion.cos.exceptions.ReturnTypesDontMatchException;
import nl.saxion.cos.model.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.ArrayList;
import java.util.List;

public class Checker extends ChellBaseVisitor<DataType> {
    private ParseTreeProperty<DataType> types;
    private ParseTreeProperty<Symbol> symbols;
    private Scope currentScope;

    private DataType registerType(ParseTree node, DataType type) {
        types.put(node, type);
        return type;
    }

    public Checker(ParseTreeProperty<DataType> types, ParseTreeProperty<Symbol> symbols) {
        this.currentScope = new Scope(null);
        this.types = types;
        this.symbols = symbols;
    }

    private SymbolVariable registerSymbol(ParseTree node, SymbolVariable symbol) {

        symbols.put(node, symbol);
        return symbol;
    }

    private SymbolMethod registerMethod(ParseTree node, SymbolMethod symbol) {
        symbols.put(node, symbol);
        return symbol;
    }


    @Override
    public DataType visitProject(ChellParser.ProjectContext ctx) {

        for (int i = 0; i < ctx.method().size(); i++) {
            visit(ctx.method(i));
        }

        visit(ctx.statements());

        return null;
    }

    public ParseTreeProperty<DataType> getTypes() {
        return this.types;
    }

    @Override
    public DataType visitMethod(ChellParser.MethodContext ctx) {

        currentScope = currentScope.openScope();

        List<DataType> paramTypes = new ArrayList<>();

        if (ctx.declareVariableNameStatement() != null) {
            for (var declareExperssion : ctx.declareVariableNameStatement()) {
                paramTypes.add(visit(declareExperssion));
            }
        }

        visit(ctx.statements());

        DataType returnType = null;

        // check if both typename and return are there. or neither.
        if (ctx.TYPENAME() == null && ctx.expression() == null) {

        } else {

            if (ctx.TYPENAME() == null && ctx.expression() != null) {
                throw new ReturnTypesDontMatchException("return type not declared");
            }

            returnType = util.getTypeFromString(ctx.TYPENAME().getText());

            if (returnType != null && ctx.expression() == null) {
                throw new ReturnTypesDontMatchException("return not declared");
            }

            if (returnType != visit(ctx.expression())) {
                throw new ReturnTypesDontMatchException("return type and the data type being returned to not match");
            }
        }

        currentScope = currentScope.closeScope();

        currentScope.declareMethod(ctx.NAME().getText(), returnType, paramTypes);

        return null;

    }

    @Override
    public DataType visitStatDeclareVarName(ChellParser.StatDeclareVarNameContext ctx) {

        DataType type = util.getTypeFromString(ctx.TYPENAME().getText());

        SymbolVariable symvar = currentScope.declareVariable(ctx.NAME().getText(), type);

        registerSymbol(ctx, symvar);

        return symvar.getType();
    }

    @Override
    public DataType visitExNegate(ChellParser.ExNegateContext ctx) {
        return registerType(ctx, DataType.INT);
    }

    @Override
    public DataType visitExMulOp(ChellParser.ExMulOpContext ctx) {
        visit(ctx.left);
        visit(ctx.right);
        return registerType(ctx, DataType.INT);
    }

    @Override
    public DataType visitExParentHaakjes(ChellParser.ExParentHaakjesContext ctx) {
        return registerType(ctx, visit(ctx.expression()));
    }

    @Override
    public DataType visitExAddOp(ChellParser.ExAddOpContext ctx) {
        visit(ctx.left);
        visit(ctx.right);
        return registerType(ctx, DataType.INT);
    }

    @Override
    public DataType visitExStrLiteral(ChellParser.ExStrLiteralContext ctx) {
        return registerType(ctx, DataType.STRING);
    }

    @Override
    public DataType visitExBoolLiteral(ChellParser.ExBoolLiteralContext ctx) {
        return registerType(ctx, DataType.BOOLEAN);
    }

    @Override
    public DataType visitExIntLiteral(ChellParser.ExIntLiteralContext ctx) {
        return registerType(ctx, DataType.INT);
    }

    public DataType visitStatReDeclaration(ChellParser.StatReDeclarationContext ctx) {
        visit(ctx.expression());
        SymbolVariable symvar = (SymbolVariable) currentScope.lookupVariable(ctx.NAME().getText());

        registerSymbol(ctx, symvar);

        return symvar.getType();
    }

    @Override
    public DataType visitStatements(ChellParser.StatementsContext ctx) {

        for (int i = 0; i < ctx.statement().size(); i++) {
            visit(ctx.statement(i));
        }

        return null;
    }

    @Override
    public DataType visitStatement(ChellParser.StatementContext ctx) {
        return super.visitStatement(ctx);
    }

    @Override
    public DataType visitStatDeclaration(ChellParser.StatDeclarationContext ctx) {

        visit(ctx.expression());

        DataType type = util.getTypeFromString(ctx.TYPENAME().getText());

        if (type != visit(ctx.expression())) {
            throw new CompilerException("Type " + ctx.TYPENAME().getText() + " does not match the type of " + ctx.expression().getText());
        }

        SymbolVariable symvar = currentScope.declareVariable(ctx.NAME().getText(), type);

        registerSymbol(ctx, symvar);

        return registerType(ctx, symvar.getType());
    }

    @Override
    public DataType visitStatIf(ChellParser.StatIfContext ctx) {


        if (visit(ctx.exFirst) != DataType.BOOLEAN) {
            throw new CompilerException("Must be a Boolean " + visit(ctx.exFirst));
        }

        for (int i = 0; i < ctx.exInner.size(); i++) {
            if (visit(ctx.exInner.get(i)) != DataType.BOOLEAN) {
                throw new CompilerException("Must be a Boolean ");
            }
        }

        visit(ctx.statFirst);

        for (int i = 0; i < ctx.statInner.size(); i++) {
            visit(ctx.statInner.get(i));
        }

        if (ctx.statLast != null) {
            visit(ctx.statLast);
        }


        return registerType(ctx, DataType.BOOLEAN);
    }

    @Override
    public DataType visitExIdentifier(ChellParser.ExIdentifierContext ctx) {

        SymbolVariable symbol = (SymbolVariable) currentScope.lookupVariable(ctx.NAME().getText());

        if (symbol != null) {
            symbols.removeFrom(ctx);

            registerSymbol(ctx, symbol);

            return registerType(ctx, symbol.getType());
        }

        throw new CompilerException("no symbol found in ExIdentifier with name " + ctx.NAME().getText());

    }

    @Override
    public DataType visitExMethodCall(ChellParser.ExMethodCallContext ctx) {

        SymbolMethod method = (SymbolMethod) currentScope.lookupVariable(ctx.NAME().getText());

        //check of the opgegeven parameter typen kloppen met de parameter typen die de methode verwacht.

        if (method != null) {
            symbols.removeFrom(ctx);

            for (var ex : ctx.expression()) {
                visit(ex);
            }

            registerMethod(ctx, method);

            return registerType(ctx, method.getReturnType());
        }

        throw new CompilerException("no method found in exMethodCall with name " + ctx.NAME().getText());
    }


    @Override
    public DataType visitExInput(ChellParser.ExInputContext ctx) {

        return registerType(ctx, DataType.STRING);
    }

    @Override
    public DataType visitExEqualsE(ChellParser.ExEqualsEContext ctx) {

        visit(ctx.left);
        visit(ctx.right);

        DataType leftType = visit(ctx.left);
        DataType rightType = visit(ctx.right);

        if (leftType == DataType.BOOLEAN || leftType == DataType.INT && rightType == DataType.BOOLEAN || rightType == DataType.INT) {
            return registerType(ctx, DataType.BOOLEAN);
        }

        throw new CompareException("Must be an integer. (potatos) " + visit(ctx.left) + " " + visit(ctx.right));

    }

    @Override
    public DataType visitAskForInputExpression(ChellParser.AskForInputExpressionContext ctx) {
        return super.visitAskForInputExpression(ctx);
    }

    @Override
    public DataType visitStatWhile(ChellParser.StatWhileContext ctx) {

        if (visit(ctx.expression()) != DataType.BOOLEAN) {
            throw new CompilerException("This should have a cake. (it should be a boolean)");
        }
        visit(ctx.statements());

        return DataType.BOOLEAN;
    }

    @Override
    public DataType visitStatOut(ChellParser.StatOutContext ctx) {

        visit(ctx.expression());

        return registerType(ctx, DataType.STRING);
    }


}
