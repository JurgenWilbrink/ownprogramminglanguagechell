grammar Chell;

    project:
        PROGRAMSTART
        method*
        statements
        PROGRAMEND
    ;

statements:
    (statement SS)*
;

method:
    CREATEMETHOD SPACE NAME
    (COMMA SHOULDREPLYWITH SPACE TYPENAME)?
    (COMMA SETPERSONALITYWITHSETTINGS SPACE (declareVariableNameStatement) (COMMA declareVariableNameStatement)*)?
    COLON statements (RETURN expression SS)? ME
;

// Statments -------------------------------------------------------------------------
statement: // Doet iets
    ifStatement
    | outputStatement
    | whileStatement
    | setValueStatement
    | assignmentStatement
    | expression
;

setValueStatement:
    TYPENAME SPACE NAME EQSIGN expression                                       # StatDeclaration
;

assignmentStatement:
    NAME EQSIGN expression                                                      # StatReDeclaration
;

declareVariableNameStatement:
    TYPENAME SPACE NAME                                                         # StatDeclareVarName
;

ifStatement:
    PORTAL SPACE exFirst=expression COLON statFirst=statements ME
    (SPACE NEXTPORTAL SPACE exInner+=expression COLON statInner+=statements ME)*
    (SPACE NOWIWANTYOUGONE COLON statLast=statements ME)?                          # StatIf
;

whileStatement:
    TURRETKEEPSONSHOOTINGUNTIL SPACE expression COLON statements ME                # StatWhile
;

outputStatement:
    IVEGOTANOTEHERE COLON SPACE expression                                         # StatOut
;

// Expressions -------------------------------------------------------------------------
expression: // Heeft resultaat
  PARENTBRACKET expression PARENTBRACKET                                        # ExParentHaakjes
  | NAME ROUNDBRACKETOPEN (expression (COMMA expression)*)? ROUNDBRACKETCLOSE   # ExMethodCall
  | NAME                                                                        # ExIdentifier
  | STRING                                                                      # ExStrLiteral
  | MINUSWITHOUTSPACE expression                                                # ExNegate
  |  ROUNDBRACKETOPEN left=expression SPACE
     op=equalsSigning SPACE right=expression ROUNDBRACKETCLOSE                  # ExEqualsE
  | BOOLEAN                                                                     # ExBoolLiteral
  | INT                                                                         # ExIntLiteral
  | askForInputExpression                                                       # ExInput
  | left=expression SPACE ML SPACE right=expression                             # ExMulOp
  | left=expression op=PM right=expression                                      # ExAddOp
;

equalsSigning:
    EQUALS | NOTEQ | AND | OR | BT | LT | BTEQ | LTEQ //| ML | PM
;

askForInputExpression:
    ASKFORSTRINGINPUT
;


// Non-nogwat -------------------------------------------------------------------------

BOOLEAN: 'false' | 'true';
INT:  ('0' | ('-')? [1-9][0-9]*);
STRING: '"' ([A-Za-z0-9_!?,: ])* '"';

TYPENAME: INTNAME | STRINGNAME | BOOLEANNAME;

BOOLEANNAME: 'cake';
INTNAME: 'potatos';
STRINGNAME: 'notes';

RETURN: 'chapter3 ';

EQUALS: 'is not a lie';
NOTEQ: 'is a lie';
AND: 'end';
OR: 'ore';
BT: 'higher level of';
LT: 'lower level of';
BTEQ: 'higher then or on level';
LTEQ: 'lower then or on level';

SS: '?';
ME: '##';
PM: PLUS | MINUS;
ML: '*' ;

SPACE: ' ';

EQSIGN: SPACE '=' SPACE;

PLUS: SPACE '+' SPACE;
MINUS: SPACE '-' SPACE;
MINUSWITHOUTSPACE: '-';

COLON: ':' ;

PARENTBRACKET: '|' ;
ROUNDBRACKETOPEN: '(' ;
ROUNDBRACKETCLOSE: ')' ;
COMMA: ',' SPACE;

CREATEMETHOD: 'create core';
SHOULDREPLYWITH: 'should reply with';                           //give returnType
SETPERSONALITYWITHSETTINGS: 'set personnality with settings';   //Give Parameters
PORTAL: 'portal';                                               //if
NEXTPORTAL: 'next portal';                                      //else if
NOWIWANTYOUGONE: 'now i want you gone';                         //else
TURRETKEEPSONSHOOTINGUNTIL: 'turret keeps on shooting until';   //while
IVEGOTANOTEHERE: 'Ive got a note here';                         //System.out.print
PROGRAMSTART: 'IM MAKING A NOTE HERE';
PROGRAMEND: 'This was a triumph';
ASKFORSTRINGINPUT: 'Give me some notes';

NAME: [A-Za-z_]+;

WS: [ \r\n\t]+ -> skip;

COMMENT: 'burn:'.*? [\n\r]+ -> skip;